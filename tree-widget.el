;;; tree-widget.el --- Tree widget  -*- lexical-binding:t -*-

;; Copyright (C) 2004-2025 David Ponce

;; Author: David Ponce <da_vid@orange.fr>
;; Created: 16 Feb 2001
;; Keywords: extensions

;; This file is not part of GNU Emacs

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; This library provide a tree widget useful to display data
;; structures organized in a hierarchical order.
;;
;; The following properties are specific to the tree widget:
;;
;; :open
;;    Set to non-nil to expand the tree.  By default the tree is
;;    collapsed.
;;
;; :node
;;    Specify the widget used to represent the value of a tree node.
;;    By default this is an `item' widget which displays the
;;    tree-widget :tag property value if defined, or a string
;;    representation of the tree-widget value.
;;
;; :keep
;;    Specify a list of properties to keep when the tree is collapsed
;;    so they can be recovered when the tree is expanded.  This
;;    property can be used in child widgets too.
;;
;; :expander (obsoletes :dynargs)
;;    Specify a function to be called to dynamically provide the
;;    tree's children in response to an expand request.  This function
;;    will be passed the tree widget and must return a list of child
;;    widgets.  Child widgets returned by the :expander function are
;;    stored in the :args property of the tree widget.
;;
;; :expander-p
;;    Specify a predicate which must return non-nil to indicate that
;;    the :expander function above has to be called.  By default, to
;;    speed up successive expand requests, the :expander-p predicate
;;    return non-nil when the :args value is nil.  So, by default, to
;;    refresh child values, it is necessary to set the :args property
;;    to nil, then redraw the tree.
;;
;; :open-icon  (default `tree-widget-open-icon')
;; :close-icon (default `tree-widget-close-icon')
;; :empty-icon (default `tree-widget-empty-icon')
;; :leaf-icon  (default `tree-widget-leaf-icon')
;;    Those properties define the icon widgets associated to tree
;;    nodes.  Icon widgets must derive from the `tree-widget-icon'
;;    widget.  The :tag and :glyph-name property values are
;;    respectively used when drawing the text and graphic
;;    representation of the tree.  The :tag value must be a string
;;    that represent a node icon, like "[+]" for example.  The
;;    :glyph-name value must the name of an image found in the current
;;    theme, like "close" for example (see also the variable
;;    `tree-widget-theme').  Here are the default text representation
;;    of icons:
;;
;;    open-icon    "[-]"
;;    close-icon   "[+]"
;;    empty-icon   "[X]"
;;    leaf-icon    ""
;;
;; :guides     (default `tree-widget-old-guides')
;;    Define the widget which draws the tree guide lines.  See the
;;    `tree-widget-guides' widget for details.
;;
;;    The default `tree-widget-old-guides' widget draws guide lines
;;    via these guide item widgets (for compatibility, properties can
;;    also be specified in the tree-widget definition):
;;
;;    Property          Widget                         Tag
;;    ----------------  -----------------------------  ------
;;    :guide            `tree-widget-guide'            " |"
;;    :end-guide        `tree-widget-end-guide'        "  "
;;    :no-guide         `tree-widget-no-guide'         " `"
;;    :handle           `tree-widget-handle'           "-"
;;    :no-handle        `tree-widget-no-handle'        " "
;;
;;    The text representation of a tree looks like this:
;;
;;    [-] 1        (open-icon :node)
;;     |-[+] 1.0   (guide+handle+close-icon :node)
;;     |-[X] 1.1   (guide+handle+empty-icon :node)
;;     `-[-] 1.2   (end-guide+handle+open-icon :node)
;;        |- 1.2.1 (no-guide+no-handle+guide+handle+leaf-icon leaf)
;;        `- 1.2.2 (no-guide+no-handle+end-guide+handle+leaf-icon leaf)
;;
;;    The new `tree-widget-new-guides' widget provides a simplified
;;    and faster to draw guides scheme with these guide item widgets:

;;    Property          Widget                         Tag
;;    ----------------  -----------------------------  ------
;;    :mid-guide        `tree-widget-mid-guide'        " | "
;;    :skip-guide       `tree-widget-skip-guide'       "   "
;;    :node-guide       `tree-widget-node-guide'       " |-"
;;    :last-node-guide  `tree-widget-last-node-guide'  " `-"
;;
;;    The text representation of a tree looks like:
;;
;;    [-] 1        (open-icon :node)
;;     |-[+] 1.0   (node-guide+close-icon :node)
;;     |-[X] 1.1   (node-guide+empty-icon :node)
;;     `-[-] 1.2   (last-node-guide+open-icon :node)
;;        |- 1.2.1 (skip-guide+node-guide+leaf-icon leaf)
;;        `- 1.2.2 (skip-guide+last-node-guide+leaf-icon leaf)
;;
;; By default, images will be used instead of strings to draw a
;; nice-looking tree.  See the `tree-widget-image-enable',
;; `tree-widget-themes-directory', and `tree-widget-theme' options for
;; more details.

;;; Code:
(require 'wid-edit)
(require 'svg)

;;; Customization
;;
(defgroup tree-widget nil
  "Customization support for the Tree Widget library."
  :version "22.1"
  :group 'widgets)

(defcustom tree-widget-image-enable t
  "Non-nil means that tree-widget will try to use images."
  :type  'boolean
  :version "27.1")

(defvar tree-widget-themes-load-path
  '(image-load-path)
  "List of locations in which to search for the themes sub-directory.
Each element is an expression that returns a single directory or a
list of directories.  By default search in `image-load-path'.")

(defcustom tree-widget-themes-directory "tree-widget"
  "Name of the directory in which to look for an image theme.
When nil use the directory where the tree-widget library is located.
When it is a relative name, search in all occurrences of that sub
directory in the path specified by `tree-widget-themes-load-path'.
The default is to use the \"tree-widget\" relative name."
  :type '(choice (const :tag "Default" "tree-widget")
                 (const :tag "Where is this library" nil)
                 (directory :format "%{%t%}:\n%v")))

(defcustom tree-widget-theme nil
  "Name of the theme in which to look for images.
This is a sub directory of the themes directory specified by the
`tree-widget-themes-directory' option.
The default theme is \"default\".  When an image is not found in a
theme, it is searched in its parent theme.

A complete theme must at least contain images with these file names
with a supported extension (see also `tree-widget-image-formats'):

\"guide\"
  A vertical guide line.
\"no-guide\"
  An invisible vertical guide line.
\"end-guide\"
  End of a vertical guide line.
\"handle\"
  Horizontal guide line that joins the vertical guide line to an icon.
\"no-handle\"
  An invisible handle.

Plus images whose name is given by the :glyph-name property of the
icon widgets used to draw the tree.  By default these images are used:

\"open\"
  Icon associated to an expanded tree.
\"close\"
  Icon associated to a collapsed tree.
\"empty\"
  Icon associated to an expanded tree with no child.
\"leaf\"
  Icon associated to a leaf node."
  :type '(choice (const  :tag "Default" nil)
                 (string :tag "Name")))

(defcustom tree-widget-image-properties-emacs
  '(:ascent center :mask (heuristic t))
  "Default properties of Emacs images."
  :type 'plist)

(defvar tree-widget--space nil)

(defsubst tree-widget-space ()
  "Insert space at point."
  (widget-insert tree-widget--space))

(defcustom tree-widget-space-width 0.5
  "Amount of space between an icon image and a node widget.
Must be a valid space :width display property.
See Info node `(elisp)Specified Space'."
  :type
  '(choice
    (number :tag "Space width, fraction of default frame font width")
    (list   :tag "Space width, absolute number of pixels"
            :format "%{%t%}: %v" (natnum :format "%v")))
  :set (lambda (var val)
         (custom-set-default var val)
         (setq tree-widget--space
               (propertize " " 'display `(space :width ,val)))))

(defface tree-widget-guide-face
  '((t (:foreground "grey90")))
  "Default face used for text guides.")

(defface tree-widget-icon-face
  '((t (:foreground "darkgrey")))
  "Default face used for text icons.")

;;; Image support
;;
(defsubst tree-widget-use-image-p ()
  "Return non-nil if image support is currently enabled."
  (and widget-image-enable
       tree-widget-image-enable
       (display-images-p)))

(defsubst tree-widget-create-image (type file &optional props)
  "Create an image of type TYPE from FILE, and return it.
Give the image the specified properties PROPS."
  (declare (obsolete create-image "27.1"))
  (apply #'create-image `(,file ,type nil ,@props)))

(defsubst tree-widget-image-formats ()
  "Return the alist of image formats/file name extensions.
See also the option `widget-image-conversion'."
  (delq nil
        (mapcar
         (lambda (fmt)
           (and (image-type-available-p (car fmt)) fmt))
         widget-image-conversion)))

(defsubst tree-widget--tag-for-image (widget)
  "Ensure WIDGET :tag value can hold image display property.
That is, if tag is nil or an empty string, set it to a single space."
  (and (tree-widget-use-image-p)
       (equal "" (or (widget-get widget :tag) ""))
       (eq 'image (car-safe (widget-get widget :tag-glyph)))
       (widget-put widget :tag " ")))

;; Buffer local cache of theme data.
;; Vector of 4 elements:
;; 0 - chain of theme names. 1st is the current theme.
;; 1 - path (list of directories) where to search for a theme (a
;;     sub-directory).
;; 2 - image properties for the theme.
;; 3 - cache of theme's images. An alist of (image-name . image).
;;
(defvar-local tree-widget--theme nil
  "Three widget theme internal data.")

;; Low level API to to abstract access to theme elements.
(defsubst tree-widget--theme-init ()
  "Initialize theme internal data."
  (setq tree-widget--theme (make-vector 4 nil)))

(defsubst tree-widget--theme-names ()
  "Return the list of current actives themes.
Use (setf (tree-widget--theme-names) value) to set the value."
  (aref tree-widget--theme 0))
(gv-define-setter tree-widget--theme-names (name)
  `(aset tree-widget--theme 0 ,name))

(defsubst tree-widget--theme-path ()
  "Return the list of directories where theme images are looked up.
Use (setf (tree-widget--theme-path) value) to set the value."
  (aref tree-widget--theme 1))
(gv-define-setter tree-widget--theme-path (path)
  `(aset tree-widget--theme 1 ,path))

(defsubst tree-widget--theme-properties ()
  "Return the list of properties applied to theme images.
Use (setf (tree-widget--theme-properties) value) to set the value."
  (aref tree-widget--theme 2))
(gv-define-setter tree-widget--theme-properties (props)
  `(aset tree-widget--theme 2 ,props))

(defsubst tree-widget--theme-images ()
  "Return the cache of theme images.
Use (setf (tree-widget--theme-images) value) to set the value."
  (aref tree-widget--theme 3))
(gv-define-setter tree-widget--theme-images (images)
  `(aset tree-widget--theme 3 ,images))

(defsubst tree-widget-theme-name ()
  "Return the current theme name, or nil if no theme is active."
  (and (vectorp tree-widget--theme)
       (car (tree-widget--theme-names))))

(defun tree-widget-set-parent-theme (name)
  "Set to NAME the parent theme of the current theme.
The default parent theme is the \"default\" theme."
  (unless (member name (tree-widget--theme-names))
    (setf (tree-widget--theme-names)
          (append (tree-widget--theme-names) (list name)))
    ;; Load the theme setup from the first directory where the theme
    ;; is found.
    (catch 'found
      (dolist (dir (tree-widget-themes-path))
        (setq dir (expand-file-name name dir))
        (when (file-accessible-directory-p dir)
          (throw 'found
                 (load (expand-file-name
                        "tree-widget-theme-setup" dir)
                       t)))))))

(defun tree-widget-set-theme (&optional name)
  "In the current buffer, set the theme to use for images.
The current buffer must be where the tree widget is drawn.
Optional argument NAME is the name of the theme to use.  It defaults
to the value of the variable `tree-widget-theme'.
Does nothing if NAME is already the current theme.

If there is a \"tree-widget-theme-setup\" library in the theme
directory, load it to setup a parent theme or the images properties.
Typically it should contain something like this:

  (tree-widget-set-parent-theme \"my-parent-theme\")
  (tree-widget-set-image-properties
     \\='(:ascent center :mask (heuristic t)))"
  (or name (setq name (or tree-widget-theme "default")))
  (unless (string-equal name (tree-widget-theme-name))
    (tree-widget--theme-init)
    (tree-widget-set-parent-theme name)
    (tree-widget-set-parent-theme "default")))

(defun tree-widget--locate-sub-directory (name path)
  "Locate all occurrences of the sub-directory NAME in PATH.
Return a list of absolute directory names in reverse order, or nil if
not found."
  (let ((found '()))
    (dolist (elt path)
      (with-demoted-errors "In tree-widget--locate-sub-directory: %S"
        (let ((dirs (eval elt t)))
          (dolist (dir (if (listp dirs) dirs (list dirs)))
            (and (file-accessible-directory-p
                  (setq dir (expand-file-name name dir)))
                 (push dir found))))))
    found))

(defun tree-widget-themes-path ()
  "Return the path where to search for a theme.
It is specified in variable `tree-widget-themes-directory'.
Return a list of absolute directory names, or nil when no directory
has been found accessible."
  (let ((path (tree-widget--theme-path)))
    (cond
     ;; No directory was found.
     ((eq path 'void) nil)
     ;; The list of directories is available in the cache.
     (path)
     ;; Use the directory where this library is located.
     ((null tree-widget-themes-directory)
      (when (setq path (locate-library "tree-widget"))
        (setq path (file-name-directory path))
        (setq path (and (file-accessible-directory-p path)
                        (list path)))
        ;; Store the result in the cache for later use.
        (setf (tree-widget--theme-path) (or path 'void))
        path))
     ;; Check accessibility of absolute directory name.
     ((file-name-absolute-p tree-widget-themes-directory)
      (setq path (expand-file-name tree-widget-themes-directory))
      (setq path (and (file-accessible-directory-p path)
                      (list path)))
      ;; Store the result in the cache for later use.
      (setf (tree-widget--theme-path) (or path 'void))
      path)
     ;; Locate a sub-directory in `tree-widget-themes-load-path'.
     (t
      (setq path (nreverse (tree-widget--locate-sub-directory
                            tree-widget-themes-directory
                            tree-widget-themes-load-path)))
      ;; Store the result in the cache for later use.
      (setf (tree-widget--theme-path) (or path 'void))
      path))))

(defvar tree-widget-pointer-shape 'arrow
  "Pointer shape when the mouse pointer is over tree-widget images.")

(defsubst tree-widget-set-image-properties (props)
  "In current theme, set images properties to PROPS.
Does nothing if images properties have already been set for that
theme."
  (or (tree-widget--theme-properties)
      (setf (tree-widget--theme-properties) props)))

(defsubst tree-widget-image-properties (_name)
  "Return the properties of image NAME in current theme.
Default global properties are provided in the variable
`tree-widget-image-properties-emacs'."
  ;; Add the pointer shape
  (cons :pointer
        (cons (or tree-widget-pointer-shape 'arrow)
              (tree-widget-set-image-properties
               tree-widget-image-properties-emacs))))

(defun tree-widget-lookup-image (name)
  "Look up in current theme for an image with NAME.
Search first in current theme, then in parent themes (see also the
function `tree-widget-set-parent-theme').  Return the first image
found having a supported format, or nil if not found."
  (catch 'found
    (dolist (default-directory (tree-widget-themes-path))
      (dolist (dir (tree-widget--theme-names))
        (dolist (fmt (tree-widget-image-formats))
          (dolist (ext (cdr fmt))
            (let ((file (expand-file-name (concat name ext) dir)))
              (and (file-readable-p file)
                   (file-regular-p file)
                   (throw 'found
                          (apply
                           #'create-image file (car fmt) nil
                           (tree-widget-image-properties name)))))))))
      nil))

(defun tree-widget-merge-image-attributes (image props)
  "From IMAGE, return a new image with merged PROPS attributes.
When an attribute in PROPS exists in IMAGE, the value in PROPS replace
the value in IMAGE.  If the attribute value in PROPS is nil, the
corresponding attribute is removed from IMAGE.  Attributes in PROPS
not in IMAGE are added to IMAGE."
  (and props
       (setq image (copy-sequence image))
       (while props
         (setf (image-property image (car props)) (cadr props))
         (setq props (cddr props))))
  image)

(defun tree-widget-find-image (name &optional props)
  "Return image with NAME found in current theme, or nil if not found.
NAME is an image file name sans extension.  Default image attributes
are defined in the variable `tree-widget-image-properties-emacs'.
Optional PROPS are additional image attributes applied to the image."
  (when (tree-widget-use-image-p)
    ;; Ensure there is an active theme.
    (tree-widget-set-theme (tree-widget-theme-name))
    ;; First lookup image in the cache, then in theme path.
    (let ((image (cdr (assoc name (tree-widget--theme-images)))))
      (or image
          (and (setq image (tree-widget-lookup-image name))
               ;; Only cache image with global default attributes.
               (push (cons name image) (tree-widget--theme-images))))
      ;; Eventually apply passed additional attributes.
      (tree-widget-merge-image-attributes image props))))

;;; SVG guides drawing
;;
(defvar-local tree-widget--root-tree-pos nil
  "Position of the current root tree.")

(defvar-local tree-widget--svg-width nil
  "Width in pixels of a generated image for a svg guide.
That is, the width of the 1st node icon of the current root tree.")

(defvar-local tree-widget--svg-height nil
  "Height in pixels of generated svg guide images.
That is, the height of the 1st line of the current root tree.")

(defvar-local tree-widget--svg-cache nil
  "Local cache of generated svg guide images.")

(defconst tree-widget--svg-stroke-width     0.5)
(defconst tree-widget--svg-stroke-dasharray "1,2")

(defun tree-widget--svg-draw-mid-guide (w h &optional color fill)
  "Draw and return a svg image for a mid guide.
W and H are respectively the width and height of the image.  Optional
argument COLOR is the guide color.  If nil, default to the foreground
color of the default face.  Optional argument FILL is the image
background color.  If nil default to the background color of the
default face."
  (let ((svg (svg-create
              w h
              :fill-color (or fill (face-background 'default))
              :stroke (or color (face-foreground 'default))
              :stroke-width tree-widget--svg-stroke-width
              :stroke-dasharray tree-widget--svg-stroke-dasharray
              )))
    (svg-line svg (/ w 2) 0 (/ w 2) h)
    svg))

(defun tree-widget--svg-draw-node-guide (w h &optional color fill)
  "Draw and return a svg image for a node guide.
W and H are respectively the width and height of the image.  Optional
argument COLOR is the guide color.  If nil, default to the foreground
color of the default face.  Optional argument FILL is the image
background color.  If nil default to the background color of the
default face."
  (let ((svg (svg-create
              w h
              :fill-color (or fill (face-background 'default))
              :stroke (or color (face-foreground 'default))
              :stroke-width tree-widget--svg-stroke-width
              :stroke-dasharray tree-widget--svg-stroke-dasharray
              ))
        ;; Slightly shift on the right the horizontal part of the
        ;; guide line for a better appearance with dotted lines.
        (hbar-margin (* 2 tree-widget--svg-stroke-width)))
    (svg-line svg (/ w 2) 0 (/ w 2) h)
    (svg-line svg (+ hbar-margin (/ w 2)) (/ h 2) w (/ h 2))
    svg))

(defun tree-widget--svg-draw-last-node-guide (w h &optional color fill)
  "Draw and return a svg image for a last node guide.
W and H are respectively the width and height of the image.  Optional
argument COLOR is the guide color.  If nil, default to the foreground
color of the default face.  Optional argument FILL is the image
background color.  If nil default to the background color of the
default face."
  (let ((svg (svg-create
              w h
              :fill-color (or fill (face-background 'default))
              :stroke (or color (face-foreground 'default))
              :stroke-width tree-widget--svg-stroke-width
              :stroke-dasharray tree-widget--svg-stroke-dasharray
              ))
        ;; Slightly shift on the right the horizontal part of the
        ;; guide line for a better appearance with dotted lines.
        (hbar-margin (* 2 tree-widget--svg-stroke-width)))
    (svg-line svg (/ w 2) 0 (/ w 2) (/ h 2))
    (svg-line svg (+ hbar-margin (/ w 2)) (/ h 2) w (/ h 2))
    svg))

(defun tree-widget--svg-draw-skip-guide (w h &optional color fill)
  "Draw and return a svg image for a skip (empty) guide.
W and H are respectively the width and height of the image.  Optional
argument COLOR is the guide color.  If nil, default to the foreground
color of the default face.  This color is not used in the skip guide.
Optional argument FILL is the image background color.  If nil default
to the background color of the default face."
  (let ((svg (svg-create
              w h
              :fill-color (or fill (face-background 'default))
              :stroke (or color (face-foreground 'default))
              )))
    svg))

;;; Widgets
;;
(defun tree-widget-button-click (event)
  "Move to the position clicked on, and if it is a button, invoke it.
EVENT is the mouse event received."
  (interactive "e")
  (mouse-set-point event)
  (let ((pos (widget-event-point event)))
    (if (get-char-property pos 'button)
        (widget-button-click event))))

(defvar tree-widget-button-keymap
  (let ((km (make-sparse-keymap)))
    (set-keymap-parent km widget-keymap)
    (define-key km [down-mouse-1] 'tree-widget-button-click)
    km)
  "Keymap used inside node buttons.
Handle mouse button 1 click on buttons.")

;;; Icons
;;
(define-widget 'tree-widget-icon 'push-button
  "Basic widget other tree-widget icons are derived from.
This widget is derived from `push-button' with the below additional
properties:

:glyph-name   - name of the image for the graphic representation of
                the icon.

:glyph-width  - width in pixels of the icon image.
                Default to the current width of icon image.

:glyph-height - height in pixels of the icon image.
                Default to the current height of icon image.

:tag          - static text representation of the icon, when images
                are not used.

:tag-face     - :tag text face.  Default is `tree-widget-icon-face'.
                Override the :button-face of the `push-button' widget.

:customize    - function that is passed the icon widget to dynamically
                change its properties before it is created.  For
                example, to dynamically associate a specific icon to
                a node (available in the `:node' property) by changing
                the values of the `:tag' and `:glyph-name' properties
                accordingly."
  :format      "%[%t%]"
  :keymap      tree-widget-button-keymap
  :customize   #'tree-widget-icon-default-customize
  :create      #'tree-widget-icon-create
  :action      #'tree-widget-icon-action
  :help-echo   #'tree-widget-icon-help-echo
  :tag-face    'tree-widget-icon-face
  )

(defvar tree-widget-before-create-icon-functions nil
  "Hooks run before to create a tree-widget icon.
Each function is passed the icon widget not yet created.
The value of the icon widget :node property is a tree :node widget or
a leaf node widget, not yet created.
This hook can be used to dynamically change properties of the icon and
associated node widgets.  For example, to dynamically change the look
and feel of the tree-widget by changing the values of the :tag and
:glyph-name properties of the icon widget.  This hook should be local
in the buffer setup to display widgets.")

(defun tree-widget-icon-default-customize (icon)
  "Default function run to customize properties before to create ICON.
Run hooks in `tree-widget-before-create-icon-functions' for
compatibility."
  (run-hook-with-args 'tree-widget-before-create-icon-functions icon))

(defun tree-widget-icon-create (icon)
  "Create the ICON widget."
  (and (functionp (widget-get icon :customize))
       (widget-apply icon :customize))
  ;; Don't get the image if already set by the :customize function.
  (or (widget-get icon :tag-glyph)
      (let ((w (widget-get icon :glyph-width))
            (h (widget-get icon :glyph-height))
            props)
        (and w (push `(:width  ,w) props))
        (and h (push `(:height ,h) props))
        (widget-put icon :tag-glyph
                    (tree-widget-find-image
                     (widget-get icon :glyph-name)
                     props))))
  (tree-widget--tag-for-image icon)
  (widget-put icon :button-face (or (widget-get icon :tag-face)
                                    (widget-get icon :button-face)))
  (widget-default-create icon)
  (or (local-variable-p 'tree-widget--svg-width)
      (setq tree-widget--svg-width
            (string-pixel-width
             (string-trim-right
              (buffer-substring (widget-get icon :from)
                                (widget-get icon :to))))))
  ;; Insert space between the icon and the node widget.
  (tree-widget-space))

(defsubst tree-widget-leaf-node-icon-p (icon)
  "Return non-nil if ICON is a leaf node icon.
That is, if its :node property value is a leaf node widget."
  (widget-get icon :tree-widget--leaf-flag))

(defun tree-widget-icon-action (icon &optional event)
  "Handle the ICON widget :action.
If ICON :node is a leaf node it handles the :action.  The tree-widget
parent of ICON handles the :action otherwise.
Pass the received EVENT to :action."
  (let ((node (widget-get icon (if (tree-widget-leaf-node-icon-p icon)
                                   :node :parent))))
    (widget-apply node :action event)))

(defun tree-widget-icon-help-echo (icon)
  "Return the help-echo string of ICON.
If ICON :node is a leaf node it handles the :help-echo.  The
tree-widget parent of ICON handles the :help-echo otherwise."
  (let* ((node (widget-get icon (if (tree-widget-leaf-node-icon-p icon)
                                    :node :parent)))
         (help-echo (widget-get node :help-echo)))
    (if (functionp help-echo)
        (funcall help-echo node)
      help-echo)))

(define-widget 'tree-widget-open-icon 'tree-widget-icon
  "Icon for an expanded tree-widget node."
  :tag        "[-]"
  :glyph-name "open"
  )

(define-widget 'tree-widget-empty-icon 'tree-widget-icon
  "Icon for an expanded tree-widget node with no child."
  :tag        "[X]"
  :glyph-name "empty"
  )

(define-widget 'tree-widget-close-icon 'tree-widget-icon
  "Icon for a collapsed tree-widget node."
  :tag        "[+]"
  :glyph-name "close"
  )

(define-widget 'tree-widget-leaf-icon 'tree-widget-icon
  "Icon for a tree-widget leaf node."
  :tag        ""
  :glyph-name "leaf"
  )

;;; Guides
;;
(define-widget 'tree-widget-guides 'default
  "Basic widget for guides.
This widget is a container.  Derived widgets must define properties
for child widgets `tree-widget-guide-item', and provide the :create
and :convert-widget functions to handle guide drawing.  This widget
value is non-nil at the end of a vertical guide line.  See the
`tree-widget-old-guides', and `tree-widget-new-guides' widgets for
examples of implementation."
  :value-get #'widget-value-value-get
  )

(define-widget 'tree-widget-guide-item 'item
  "Basic widget other tree-widget guides are derived from.
Derived widgets must provide the :tag and :glyph-name properties for
respectively the text and graphic (image) representation of the guide
item.  Here are the recognized properties:

:glyph-name   - name of the image for the graphic representation of
                the guide item.

:glyph-width  - width in pixels of the guide element.
                Default to the current width of guide image.

:glyph-height - height in pixels of the guide element.
                Default to the current height of guide image.

:tag          - static text representation of the widget, when images
                are not used.

:tag-face     - :tag text face.  Default is `tree-widget-guide-face'.
                Override the :sample-face of the `item' widget."
  :convert-widget #'tree-widget-guide-item-convert
  :format         "%{%t%}"
  :tag-face       'tree-widget-guide-face
  )

(defun tree-widget-guide-item-convert (guide)
  "Prepare the GUIDE item widget."
  (or (widget-get guide :tag-glyph)
      (let ((w (widget-get guide :glyph-width))
            (h (widget-get guide :glyph-height))
            props)
        (and w (push `(:width  ,w) props))
        (and h (push `(:height ,h) props))
        (widget-put guide :tag-glyph
                    (tree-widget-find-image
                     (widget-get guide :glyph-name)
                     props))))
  (tree-widget--tag-for-image guide)
  (widget-put guide :sample-face (or (widget-get guide :tag-face)
                                     (widget-get guide :sample-face)))
  (widget-value-convert-widget guide))

;;; Old guide line widgets for compatibility.
;;
(define-widget 'tree-widget-old-guides 'tree-widget-guides
  "The default guides widget."
  :create         #'tree-widget-old-guides-create
  :convert-widget #'tree-widget-old-guides-convert
  :guide          'tree-widget-guide
  :end-guide      'tree-widget-end-guide
  :no-guide       'tree-widget-no-guide
  :handle         'tree-widget-handle
  :no-handle      'tree-widget-no-handle
  )

(defun tree-widget-old-guides-convert (guides)
  "Prepare the GUIDES widget."
  (let* ((widget (widget-types-convert-widget guides))
         (tree (widget-get widget :parent)))
    (dolist (i '(:guide :end-guide :no-guide :handle :no-handle))
      (widget-put widget i
                  ;; For compatibility, get guide item widgets from
                  ;; the :parent tree-widget type, then from GUIDES.
                  (widget-convert (or (widget-get tree i)
                                      (widget-get widget i)))))
    widget))

(defun tree-widget-old-guides-create (guides)
  "Create the GUIDES widget."
  ;; Insert guide lines elements from previous levels.
  (dolist (f (widget-get guides :guide-flags))
    (widget-create-child
     guides (widget-get guides (if f :guide :no-guide)))
    (widget-create-child guides (widget-get guides :no-handle)))
  ;; Insert guide line element for this level.
  (widget-create-child
   guides (widget-get guides (if (widget-value guides)
                                 :end-guide :guide)))
  ;; Insert the node handle line
  (widget-create-child guides (widget-get guides :handle)))

(define-widget 'tree-widget-guide 'tree-widget-guide-item
  "Vertical guide line."
  :tag        " |"
  :glyph-name "guide"
  )

(define-widget 'tree-widget-end-guide 'tree-widget-guide-item
  "End of a vertical guide line."
  :tag        " `"
  :glyph-name "end-guide"
  )

(define-widget 'tree-widget-no-guide 'tree-widget-guide-item
  "Invisible vertical guide line."
  :tag        "  "
  :glyph-name "no-guide"
  )

(define-widget 'tree-widget-handle 'tree-widget-guide-item
  "Horizontal guide line that joins a vertical guide line to a node."
  :tag        "-"
  :glyph-name "handle"
  )

(define-widget 'tree-widget-no-handle 'tree-widget-guide-item
  "Invisible handle."
  :tag        " "
  :glyph-name "no-handle"
  )

;;; New guide line widgets
;;
(define-widget 'tree-widget-new-guides 'tree-widget-guides
  "The alternate guides widget."
  :create          #'tree-widget-new-guides-create
  :convert-widget  #'tree-widget-new-guides-convert
  :mid-guide       'tree-widget-mid-guide
  :skip-guide      'tree-widget-skip-guide
  :node-guide      'tree-widget-node-guide
  :last-node-guide 'tree-widget-last-node-guide
  )

(defun tree-widget-new-guides-convert (guides)
  "Prepare the GUIDES widget."
  (let ((widget (widget-types-convert-widget guides)))
    (dolist (i '(:mid-guide :skip-guide :node-guide :last-node-guide))
      (widget-put widget i (widget-convert (widget-get widget i))))
    widget))

(defun tree-widget-new-guides-create (guides)
  "Create the GUIDES widget."
  ;; Insert guide lines from previous levels.
  (dolist (f (widget-get guides :guide-flags))
    (widget-create-child
     guides (widget-get guides (if f :mid-guide :skip-guide)))
    ;; (tree-widget-space)
    )
  ;; Insert node guide for this level.
  (widget-create-child
   guides (widget-get guides (if (widget-value guides)
                                 :last-node-guide :node-guide)))
  ;; (tree-widget-space)
  )

(define-widget 'tree-widget-mid-guide 'tree-widget-guide-item
  "Vertical guide line."
  :tag        " | "
  :glyph-name "mid-guide"
  )

(define-widget 'tree-widget-last-node-guide 'tree-widget-guide-item
  "End of a vertical guide line, in front of the last child node."
  :tag        " `-"
  :glyph-name "last-node-guide"
  )

(define-widget 'tree-widget-node-guide 'tree-widget-guide-item
  "Vertical guide line in front of a node."
  :tag        " |-"
  :glyph-name "node-guide"
  )

(define-widget 'tree-widget-skip-guide 'tree-widget-guide-item
  "Invisible guide line."
  :tag        "   "
  :glyph-name "skip-guide"
  )

(define-widget 'tree-widget-svg-guide 'tree-widget-guide-item
  "Basic widget used for tree-widget svg guide lines.
svg guides are dotted lines dynamically drawn as svg images.  Derived
widgets must provide at least the :tag and :glyph-name properties for
respectively the text and graphic representation of the guide.  Here
are the recognized properties:

:glyph-name   - name of the guide element, must be one of
                \"mid-guide\", \"node-guide\", \"last-node-guide\", or
                \"skip-guide\".  The `:guides' property of the parent
                tree-widget must be set to `tree-widget-new-guides'.

:glyph-color  - color of the guide element.
                Default to the foreground color of the default font.

:glyph-fill   - background color of the guide element.
                Default to the background color of the default
                font. If transparency is defined thru the :mask image
                property, the background color is meaningless.

:glyph-width  - width in pixels of the guide element.
                Default to the width of the node icon image of the
                first line of the current root tree.

:glyph-height - height in pixels of the guide element.
                Default to the line height of the first line of the
                current root tree.

:tag          - static text representation of the widget, when images
                are not used.

:tag-face     - :tag text face.  Default is `tree-widget-guide-face'.
                Override the :sample-face of the `item' widget."
  :convert-widget #'tree-widget-svg-guide-convert
  )

(defun tree-widget--svg-draw-guide (guide)
  "Draw and return an svg image for the GUIDE widget."
  (let ((guide-drawer
         (intern-soft (format "tree-widget--svg-draw-%s"
                              (widget-get guide :glyph-name)))))
    (when (fboundp guide-drawer)
      (svg-image
       (funcall guide-drawer
                (or (widget-get guide :glyph-width)
                    tree-widget--svg-width)
                (or (widget-get guide :glyph-height)
                    tree-widget--svg-height)
                (widget-get guide :glyph-color)
                (widget-get guide :glyph-fill))
       :scale 1.0
       :pointer 'arrow :ascent 'center :mask '(heuristic t)))))

(defun tree-widget-svg-guide-convert (guide)
  "Prepare the GUIDE item widget."
  (and (tree-widget-use-image-p)
       (not (widget-get guide :tag-glyph))
       (let* ((gln (widget-get guide :glyph-name))
              (img (cdr (assoc gln tree-widget--svg-cache))))
         (unless img
           (or (local-variable-p 'tree-widget--svg-height)
               (save-excursion
                 (goto-char tree-widget--root-tree-pos)
                 (setq tree-widget--svg-height (line-pixel-height))))
           (setq img (tree-widget--svg-draw-guide guide))
           (push (cons gln img) tree-widget--svg-cache))
         (and img (widget-put guide :tag-glyph img))))
  (widget-put guide :sample-face (or (widget-get guide :tag-face)
                                     (widget-get guide :sample-face)))
  (widget-value-convert-widget guide))

(define-widget 'tree-widget-svg-guides 'tree-widget-new-guides
  "The alternate guides widget."
  :mid-guide       'tree-widget-svg-mid-guide
  :skip-guide      'tree-widget-svg-skip-guide
  :node-guide      'tree-widget-svg-node-guide
  :last-node-guide 'tree-widget-svg-last-node-guide
  )

(define-widget 'tree-widget-svg-mid-guide 'tree-widget-svg-guide
  "Vertical guide line."
  :tag         " | "
  :glyph-name  "mid-guide"
  )

(define-widget 'tree-widget-svg-last-node-guide 'tree-widget-svg-guide
  "End of a vertical guide line, in front of the last child node."
  :tag         " `-"
  :glyph-name  "last-node-guide"
  )

(define-widget 'tree-widget-svg-node-guide 'tree-widget-svg-guide
  "Vertical guide line in front of a node."
  :tag         " |-"
  :glyph-name  "node-guide"
  )

(define-widget 'tree-widget-svg-skip-guide 'tree-widget-svg-guide
  "Invisible guide line."
  :tag         "   "
  :glyph-name  "skip-guide"
  )

;;; Tree widget
;;
(define-widget 'tree-widget 'default
  "Tree widget."
  :format         "%v"
  :match          #'ignore
  :convert-widget #'tree-widget-convert-widget
  :value-get      #'widget-value-value-get
  :value-delete   #'widget-children-value-delete
  :value-create   #'tree-widget-value-create
  :action         #'tree-widget-action
  :help-echo      #'tree-widget-help-echo
  :expander-p     #'tree-widget-expander-p
  :open-icon      'tree-widget-open-icon
  :close-icon     'tree-widget-close-icon
  :empty-icon     'tree-widget-empty-icon
  :leaf-icon      'tree-widget-leaf-icon
  :guides         'tree-widget-old-guides
  ;; :guides         'tree-widget-new-guides
  ;; :guides         'tree-widget-svg-guides
  )

(defun tree-widget-p (widget)
  "Return non-nil if WIDGET is a tree-widget."
  (let ((type (widget-type widget)))
    (while (and type (not (eq type 'tree-widget)))
      (setq type (widget-type (get type 'widget-type))))
    (eq type 'tree-widget)))

(defun tree-widget-node (widget)
  "Return WIDGET :node child widget.
If not found, setup an `item' widget as default.
Signal an error if the :node widget is a tree-widget.
WIDGET is, or derives from, a tree-widget."
  (let ((node (widget-get widget :node)))
    (if node
        ;; Check that the :node widget is not a tree-widget.
        (and (tree-widget-p node)
             (error "Invalid tree-widget :node %S" node))
      ;; Setup an item widget as default :node.
      (setq node `(item :tag ,(or (widget-get widget :tag)
                                  (widget-princ-to-string
                                   (widget-value widget)))))
      (widget-put widget :node node))
    node))

(defun tree-widget-keep (arg widget)
  "Save in ARG the WIDGET properties specified by :keep."
  (dolist (prop (widget-get widget :keep))
    (widget-put arg prop (widget-get widget prop))))

(defun tree-widget-children-value-save (widget &optional args node)
  "Save WIDGET children values.
WIDGET is, or derives from, a tree-widget.  Children properties and
values are saved in ARGS if non-nil, else in WIDGET :args property
value.  Properties and values of the WIDGET :node sub-widget are saved
in NODE if non-nil, else in WIDGET :node sub-widget."
  (let ((args (cons (or node (widget-get widget :node))
                    (or args (widget-get widget :args))))
        (children (widget-get widget :children))
        arg child)
    (while (and args children)
      (setq arg      (car args)
            args     (cdr args)
            child    (car children)
            children (cdr children))
       (if (tree-widget-p child)
;;;; The child is a tree node.
           (progn
             ;; Backtrack :args and :node properties.
             (widget-put arg :args (widget-get child :args))
             (widget-put arg :node (widget-get child :node))
             ;; Save :open property.
             (widget-put arg :open (widget-get child :open))
             ;; The node is open.
             (when (widget-get child :open)
               ;; Save the widget value.
               (widget-put arg :value (widget-value child))
               ;; Save properties specified in :keep.
               (tree-widget-keep arg child)
               ;; Save children.
               (tree-widget-children-value-save
                child (widget-get arg :args) (widget-get arg :node))))
;;;; Another non tree node.
         ;; Save the widget value.
         (widget-put arg :value (widget-value child))
         ;; Save properties specified in :keep.
         (tree-widget-keep arg child)))))

(defun tree-widget-convert-widget (widget)
  "Convert :args as widget types in WIDGET."
  (let ((tree (widget-types-convert-widget widget)))
    ;; Compatibility
    (widget-put tree :expander (or (widget-get tree :expander)
                                   (widget-get tree :dynargs)))
    tree))

(defun tree-widget-value-create (tree)
  "Create the TREE tree-widget."
  (let* ((node   (tree-widget-node tree))
         (indent (or (widget-get tree :indent) 0))
         ;; Setup widget image support.  Looking up for images, and
         ;; setting widgets :tag-glyph is done here, to allow to
         ;; dynamically change the image theme.
         (widget-image-enable (tree-widget-use-image-p))
         image-map ;; Do not setup image map
         children buttons)
    (or (widget-get tree :parent) (insert-char ?\s indent))
    (if (widget-get tree :open)
;;;; Expanded node.
        (let ((args
               (if (and (widget-get tree :expander)
                        (widget-apply tree :expander-p))
                   ;; Populate children at run time, when requested.
                   (widget-put
                    tree :args (mapcar 'widget-convert
                                       (widget-apply tree :expander)))
                 (widget-get tree :args)))
              (flags (widget-get tree :tree-widget--guide-flags))
              guides)
          ;; If there is no guide to draw before the current tree
          ;; (that is, current tree is a root tree), and not already
          ;; done, initialize the dimension that will be used to
          ;; generate svg guide images.
          (unless (or flags (eql tree-widget--root-tree-pos (point)))
            (setq tree-widget--root-tree-pos (point))
            (kill-local-variable 'tree-widget--svg-height)
            (kill-local-variable 'tree-widget--svg-width)
            (kill-local-variable 'tree-widget--svg-cache))
          ;; Defer the node widget creation after icon creation.
          (widget-put tree :node (widget-convert node))
          ;; Create the icon widget for the expanded tree.
          (push
           (widget-create-child-and-convert
            tree (widget-get tree (if args :open-icon :empty-icon))
            ;; Pass the node widget to child.
            :node (widget-get tree :node))
           buttons)
          ;; Create the tree node widget.
          (push (widget-create-child tree (widget-get tree :node))
                children)
          ;; Update the icon :node with the created node widget.
          (widget-put (car buttons) :node (car children))
          ;; Prepare the guides widget.
          (setq guides (widget-convert
                        ;; Make the :parent tree accessible by the
                        ;; :convert-widget function of the :guides
                        ;; widget.
                        (list (widget-get tree :guides) :parent tree)
                        :guide-flags (reverse flags)))
          ;; Create the tree children.
          (while args
            (setq node (car args)
                  args (cdr args))
            (insert-char ?\s indent)
            ;; Insert guides.
            (widget-create-child-value tree guides (null args))
            (if (tree-widget-p node)
                ;; Create a sub-tree node.
                (push (widget-create-child-and-convert
                       tree node :tree-widget--guide-flags
                       (cons (if args t) flags))
                      children)
              ;; Create the icon widget for a leaf node.
              (push (widget-create-child-and-convert
                     tree (widget-get tree :leaf-icon)
                     ;; At this point the node widget isn't yet
                     ;; created.
                     :node (setq node (widget-convert
                                       node :tree-widget--guide-flags
                                       (cons (if args t) flags)))
                     :tree-widget--leaf-flag t)
                    buttons)
              ;; Create the leaf node widget.
              (push (widget-create-child tree node) children)
              ;; Update the icon :node with the created node widget.
              (widget-put (car buttons) :node (car children)))))
;;;; Collapsed node.
      ;; Defer the node widget creation after icon creation.
      (widget-put tree :node (widget-convert node))
      ;; Create the icon widget for the collapsed tree.
      (push (widget-create-child-and-convert
             tree (widget-get tree :close-icon)
             ;; Pass the node widget to child.
             :node (widget-get tree :node))
            buttons)
      ;; Create the tree node widget.
      (push (widget-create-child tree (widget-get tree :node))
            children)
      ;; Update the icon :node with the created node widget.
      (widget-put (car buttons) :node (car children)))
    ;; Save widget children and buttons.  The tree-widget :node child
    ;; is the first element in :children.
    (widget-put tree :children (nreverse children))
    (widget-put tree :buttons  buttons)))

(defvar tree-widget-after-toggle-functions nil
  "Hooks run after toggling a tree-widget expansion.
Each function is passed a tree-widget.  If the value of the :open
property is non-nil the tree has been expanded, else collapsed.
This hook should be local in the buffer setup to display widgets.")

(defun tree-widget-action (tree &optional _event)
  "Handle the :action of the TREE tree-widget.
That is, toggle expansion of the TREE tree-widget.
Ignore the EVENT argument."
  (let ((open (not (widget-get tree :open))))
    (or open
        ;; Before to collapse the node, save children values so next
        ;; open can recover them.
        (tree-widget-children-value-save tree))
    (widget-put tree :open open)
    (save-window-excursion
      (widget-value-set tree open)
      (run-hook-with-args 'tree-widget-after-toggle-functions tree))))

(defun tree-widget-help-echo (tree)
  "Return the help-echo string of the TREE tree-widget."
  (if (widget-get tree :open)
      "Collapse node"
    "Expand node"))

(defun tree-widget-expander-p (tree)
  "Return non-nil if the TREE tree-widget :expander has to be called.
That is, if TREE :args is nil."
  (null (widget-get tree :args)))

(provide 'tree-widget)

;;; tree-widget.el ends here
