# emhacks

This repository contains Emacs Lisp libraries I maintain for my own
use.

- `recentf.el`<sup>[[1](#1)]</sup>, to keep track of recently opened
  files.
  
- `tree-widget.el`<sup>[[1](#1)]</sup>, a tree widget.

- `grind.el`, a simple Elisp pretty-printer.

- `gdiff.el`, to use a GUI diff tool (like
  [kdiff3](https://apps.kde.org/kdiff3/)) from Emacs.

<br>&HorizontalLine;&HorizontalLine;&HorizontalLine;&HorizontalLine;<br>
<sup id="1">**[1]**</sup> <small>These libraries have been integrated
into Emacs. Here are more recent versions I maintain for my own
use.</small>
