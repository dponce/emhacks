;;; ++fns.el --- Miscellaneous functions -*- lexical-binding:t -*-

;; Copyright (C) 2020-2025 David Ponce

;; Author: David Ponce <da_vid@orange.fr>
;; Maintainer: David Ponce <da_vid@orange.fr>
;; Created: 07 March 2020
;; Keywords: convenience

;; This file is not part of GNU Emacs.

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 2, or (at
;; your option) any later version.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 51 Franklin Street, Fifth
;; Floor, Boston, MA 02110-1301, USA.

;;; Commentary:
;;
;; This library provides various miscellaneous functions
;;

;;; History:
;;

;;; Code:

(require 'xdg)

(defun ++plist-del (plist prop)
  "Return PLIST with PROP and its value removed.
PLIST is a property list, which is a list of the form
\(PROP1 VALUE1 PROP2 VALUE2 ...).  PROP is a symbol."
  (cond
   ((null plist) nil)
   ((null prop) plist)
   ((eq (car plist) prop) (cddr plist))
   (t (cons (car plist)
            (cons (cadr plist) (++plist-del (cddr plist) prop))))))

(defun ++image-update-attributes (image props)
  "From IMAGE, return a new image with attributes PROPS merged.
When an attribute in PROPS exists in IMAGE, the value in PROPS replace
the value in IMAGE.  If the attribute value in PROPS is nil, the
corresponding attribute is removed from IMAGE.  Attributes in PROPS
not in IMAGE are added to IMAGE."
  (and props
       (setq image (copy-sequence image))
       (while props
         (setf (image-property image (car props)) (cadr props))
         (setq props (cddr props))))
  image)

(defun ++merge-image-properties (props merge)
  "Merge image attributes MERGE into PROPS.
Return the new plist of merged attributes.  If PROPS is nil return
MERGE.  If MERGE is nil return PROPS.  If an attribute in MERGE is
present in PROPS, replace the corresponding value in PROPS by the
value in MERGE.  If the value in MERGE is nil, the corresponding
attribute in PROPS is removed.  Attributes in MERGE not in PROPS are
added to PROPS."
  (cdr (++image-update-attributes (cons 'image props) merge)))

(defun ++xdg-cache-emacs (&optional package-name mode)
  "Return the base directory used for Emacs specific cache files.
That is, the \"emacs\" subdirectory of `xdg-cache-home'.  If optional
argument PACKAGE-NAME is non-nil, return this subdirectory of the
Emacs cache directory. The returned directory is created if it doesn't
exist.  If optional argument MODE is non-nil, it is used to set the
file permission bits for newly created directory, as for
‘set-default-file-modes’. Default MODE is u=rwx,go=."
  (let ((cache (expand-file-name "emacs" (xdg-cache-home))))
    (and package-name
         (setq cache (expand-file-name package-name cache)))
    (or (file-directory-p cache)
        (with-file-modes
            (or mode (file-modes-symbolic-to-number "u=rwx,go="))
          (make-directory cache t)))
    cache))

(defun ++locate-user-cache-file (file &optional package)
  "Return cache FILE located in `++xdg-cache-emacs'.
If optional argument PACKAGE is non-nil, FILE is located in the
corresponding subdirectory."
  (expand-file-name
   file (++xdg-cache-emacs (and package (format "%s" package)))))

(provide '++fns)

;;; ++fns.el ends here
