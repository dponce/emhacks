;;; dj.el --- Directory Jockey -*- lexical-binding:t -*-

;; Copyright (C) 2005-2025 by David Ponce

;; Author: David Ponce <da_vid@orange.fr>
;; Maintainer: David Ponce <da_vid@orange.fr>
;; Created: 5 Jul 2005
;; Keywords: extensions

;; This file is not part of Emacs

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 2, or (at
;; your option) any later version.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 51 Franklin Street, Fifth
;; Floor, Boston, MA 02110-1301, USA.

;;; Commentary:
;;
;; DJ navigates through a tree view of the file system.
;;

;;; History:
;;

;;; Code:
(require 'tree-widget)
(require 'xdg-mime)

;;; Customization
;;
(defgroup dj nil
  "Customization support for DJ."
  :group 'files)

(defcustom dj-theme "dj"
  "Name of the DJ tree-widget theme where to look up for images.
See also the variable `tree-widget-theme'."
  :type '(string :tag "Name")
  :group 'dj)

(defcustom dj-icon-theme nil
  "Theme name used to lookup mime type icons, or nil for default."
  :type '(choice
          (const  :tag "Default" nil)
          (string :tag "Icon Theme Name")))

(defcustom dj-icon-size nil
  "Size used to lookup mime type icons, or nil for default."
  :type '(choice
          (const  :tag "Default" nil)
          (natnum :tag "Icon Size")))

(defface dj-icon
  '((t (:inherit tree-widget-icon-face)))
  "Face used for text icons.")

(defface dj-guide
  '((t (:inherit tree-widget-guide-face)))
  "Face used for text guides.")

(defface dj-file
  '((t (:inherit variable-pitch)))
  "Face used for file names.")

(defface dj-directory
  '((t ( :weight bold
         :inherit dj-file)))
  "Face used for directory names.")

(defface dj-symlink
  '((t (:italic t :foreground "SteelBlue3")))
  "Face used for symbolic links.")

;;; Images & Widgets
;;
(defvar-local dj--home-on    nil)
(defvar-local dj--home-off   nil)
(defvar-local dj--bw-on      nil)
(defvar-local dj--bw-off     nil)
(defvar-local dj--fw-on      nil)
(defvar-local dj--fw-off     nil)
(defvar-local dj--up-on      nil)
(defvar-local dj--up-off     nil)
(defvar-local dj--quit-on    nil)
(defvar-local dj--link-glyph nil)

(defsubst dj--disable-image (image)
  "From IMAGE, return a new image which looks disabled."
  (and (setq image (copy-sequence image))
       (setf (image-property image :conversion) 'disabled))
  image)

(defvar-local dj--mime-type-icon-cache nil)

(defun dj--setup-images ()
  "Setup the buttons' images.
Return non-nil if all images have been setup."
  (tree-widget-set-theme dj-theme)
  ;; Home button
  (and (setq dj--home-on  (tree-widget-find-image "home"))
       (setf (image-property dj--home-on :margin) 5)
       (setq dj--home-off (dj--disable-image dj--home-on))
       (setf (image-property dj--home-off :margin) 5))
  ;; Quit button
  (and (setq dj--quit-on (tree-widget-find-image "exit"))
       (setf (image-property dj--quit-on :margin) 5))
  ;; Backward button
  (and (setq dj--bw-on   (tree-widget-find-image "backward"))
       (setf (image-property dj--bw-on :margin) 5)
       (setq dj--bw-off  (dj--disable-image dj--bw-on))
       (setf (image-property dj--bw-off :margin) 5))
  ;; Forward button
  (and (setq dj--fw-on   (tree-widget-find-image "forward"))
       (setf (image-property dj--fw-on :margin) 5)
       (setq dj--fw-off  (dj--disable-image dj--fw-on))
       (setf (image-property dj--fw-on :margin) 5))
  ;; Up button
  (and (setq dj--up-on   (tree-widget-find-image "up"))
       (setf (image-property dj--up-on :margin) 5)
       (setq dj--up-off  (dj--disable-image dj--up-on))
       (setf (image-property dj--up-off :margin) 5))
  ;; Link glyph
  (and (setq dj--link-glyph (tree-widget-find-image "link-glyph"))
       (setf (image-property dj--link-glyph :scale) 1)
       (setf (image-property dj--link-glyph :margin) 0))
  (setq dj--link-glyph (propertize " ► " 'display dj--link-glyph))
  (setq-local dj--mime-type-icon-cache
              (make-hash-table :test 'equal :size 1000))
  ;; Check that all images exist for the tool bar.
  (and dj--home-on dj--quit-on dj--bw-on dj--fw-on dj--up-on))

(define-widget 'dj-button 'push-button
  "Generic button widget."
  ;; format handler for '%>' and '%l'
  :format-handler #'dj--link-format-handler
  :format         "%[%t%]%>%{%l%}\n"
  ;; variable for the link-glyph value
  :link-glyph     'dj--link-glyph
  ;; face used for link value '%l'
  :sample-face    'dj-symlink
  )

(defun dj--link-format-handler (widget escape)
  "Handle WIDGET % ESCAPE characters in :format.
That is, additional %> and %l format used to insert :link-glyph and
:link values."
  (cond
   ((eq escape ?>) ;; %> :link-glyph format
    (and (widget-get widget :link)
         (let ((glyph (widget-get widget :link-glyph)))
           (and glyph (boundp glyph) (insert (symbol-value glyph))))))
   ((eq escape ?l) ;; %l :link format
    (and (widget-get widget :link)
         (insert (substitute-command-keys (widget-get widget :link)))))
   (t
    (widget-default-format-handler widget escape))))

(define-widget 'dj--guides 'tree-widget-svg-guides
  "The DJ guides widget."
  :mid-guide       'dj--mid-guide
  :skip-guide      'dj--skip-guide
  :node-guide      'dj--node-guide
  :last-node-guide 'dj--last-node-guide
  )

(define-widget 'dj--mid-guide 'tree-widget-svg-mid-guide
  "Vertical guide line."
  ;; :tag         "  "
  :tag         "│ "
  :tag-face    'dj-guide
  :glyph-name  nil
  )

(define-widget 'dj--last-node-guide 'tree-widget-svg-last-node-guide
  "End of a vertical guide line, in front of the last child node."
  ;; :tag         "  "
  :tag         "╰─"
  :tag-face    'dj-guide
  :glyph-name  nil
  )

(define-widget 'dj--node-guide 'tree-widget-svg-node-guide
  "Vertical guide line in front of a node."
  ;; :tag         "  "
  :tag         "├─"
  :tag-face    'dj-guide
  :glyph-name  nil
  )

(define-widget 'dj--skip-guide 'tree-widget-svg-skip-guide
  "Invisible guide line."
  :tag         "  "
  :tag-face    'dj-guide
  :glyph-name  nil
  )

(define-widget 'dj--open-icon 'tree-widget-open-icon
  "Icon for an expanded tree-widget node."
  :tag         "⌵"
  :tag-face    '(:inherit dj-icon :weight bold :foreground "green3")
  :customize   nil
  :glyph-name  nil
  )

(define-widget 'dj--close-icon 'tree-widget-close-icon
  "Icon for a collapsed tree-widget node."
  :tag         ">"
  :tag-face    '(:inherit dj-icon :weight bold :foreground "black")
  :customize   nil
  :glyph-name  nil
  )

(define-widget 'dj--empty-icon 'tree-widget-empty-icon
  "Icon for an expanded tree-widget node with no child."
  :tag         "⦰"
  :tag-face    '(:inherit dj-icon :weight bold :foreground "red3")
  :customize   nil
  :glyph-name  nil
  )

(define-widget 'dj--leaf-icon 'tree-widget-leaf-icon
  "Icon for a tree-widget leaf node."
  :tag         ""
  :tag-face    'dj-icon
  :customize   #'dj--mime-type-icon
  )

(define-widget 'dj-tree 'tree-widget
  "Directory tree widget."
  :expander    #'dj-expand-dir
  :guides      'dj--guides
  :open-icon   'dj--open-icon
  :close-icon  'dj--close-icon
  :empty-icon  'dj--empty-icon
  :leaf-icon   'dj--leaf-icon
  )

(define-widget 'dj-dir-widget 'dj-button
  "Directory node widget."
  :action      #'dj-view-or-refresh-dir
  :help-echo   "mouse-1: refresh, mouse-2: start here"
  :button-face 'dj-directory
  )

(define-widget 'dj-file-widget 'dj-button
  "File node widget."
  :action      #'dj-file-action
  :button-face 'dj-file
  )

(defsubst dj--set-state (widget flag image-on image-off)
  (widget-put widget :tag-glyph (if flag image-on image-off))
  (and (widget-get widget :tag-glyph)
       (widget-value-set widget (widget-value widget)))
  (widget-apply widget (if flag :activate :deactivate)))

(defvar-local dj--tree nil)
(defvar-local dj--home-btn nil)
(defvar-local dj--bw-btn nil)
(defvar-local dj--fw-btn nil)
(defvar-local dj--up-btn nil)

(defun dj--dir (tree)
  "Return the directory name associated to the TREE widget."
  (widget-get (widget-get tree :node) :file))

(defun dj--dir-widget (dir parent)
  "Return a widget to display a directory tree node.
PARENT and DIR are respectively the absolute names of the directory
and its parent."
  (let ((tag (file-relative-name dir parent))
        (lnk (file-symlink-p dir)))
    `(dj-tree
      :open nil ;; Create a collapsed tree
      :node (dj-dir-widget
             :file   ,dir
             :link   ,(and lnk (file-name-as-directory lnk))
             :tag    ,(if lnk tag (file-name-as-directory tag))
             ))
    ))

(defun dj--file-widget (file parent)
  "Return a widget to display a file node.
PARENT and FILE are respectively the absolute names of the file and
its parent directory."
  (let* ((tag (file-relative-name file parent))
         (lnk (file-symlink-p file)))
    `(dj-file-widget
      :file   ,file
      :link   ,lnk
      :tag    ,tag)
    ))

(defun dj--dir-as-widgets (dir)
  "Return widgets to display the content of directory DIR."
  (let (files dirs)
    (dolist (elt (directory-files dir 'full
                                  ;; exclude "." and ".." directories
                                  ;; "[^/.][.]?\\'"
                                  directory-files-no-dot-files-regexp
                                  ))
      (if (file-directory-p elt)
          (push (dj--dir-widget elt dir) dirs)
        (push (dj--file-widget elt dir) files)))
    (nreverse (nconc files dirs))))

(defsubst dj--type-icon (type)
  (let ((icon (gethash type dj--mime-type-icon-cache 'no-value)))
    (if (eq icon 'no-value)
        (puthash type
                 (and (setq icon (xdg-mime-type-icon
                                  type dj-icon-theme dj-icon-size))
                      (find-image `(( :file ,icon
                                      :margin 0
                                      :ascent center
                                      :mask (heuristic t)))))
                 dj--mime-type-icon-cache)
      icon)))

(defun dj--mime-type-icon (icon)
  "Return a mime type icon image from ICON :node properties."
  (and (tree-widget-leaf-node-icon-p icon)
       (let* ((node (widget-get icon :node))
              (link (widget-get node :link))
              (file (widget-get node :file))
              (type (xdg-mime-file-type (if link
                                            (file-truename file)
                                          file)))
              (glyph (dj--type-icon type))
              (help (format
                     "%s (%s)\nmouse-1, mouse-2: open in other window"
                     (file-name-nondirectory file)
                     (or type "unknown"))))
         (widget-put node :mime-type type)
         (widget-put icon :help-echo help)
         (widget-put node :help-echo help)
         ;; Set :glyph-name only if an image has been found, in order
         ;; to otherwise fallback to the default leaf icon.
         (and glyph (widget-put icon :tag-glyph glyph)))))

;;; The history
;;
(defvar-local dj--history nil)
(defvar-local dj--history-size  0)
(defvar-local dj--history-index 0)

(defsubst dj--not-at-home ()
  (not (string-equal (expand-file-name "~")
                     (dj--dir dj--tree))))

(defsubst dj--not-at-root ()
  "Return non-nil if DIR has a parent directory."
  ;; TODO: how to make it system independent?
  (not (string-equal "/"
                     (dj--dir dj--tree))))

(defsubst dj--history-has-next-p ()
  (> dj--history-index 0))

(defsubst dj--history-has-previous-p ()
  (< dj--history-index (1- dj--history-size)))

;; Note to self: use a hook to make the implementation generic.
(defsubst dj--history-update-buttons-state ()
  ;; Activate/Deactivate the navigation buttons.
  (when (and dj--home-btn dj--bw-btn dj--fw-btn dj--up-btn)
    (dj--set-state dj--home-btn (dj--not-at-home)
                   dj--home-on dj--home-off)
    (dj--set-state dj--bw-btn (dj--history-has-previous-p)
                   dj--bw-on dj--bw-off)
    (dj--set-state dj--fw-btn (dj--history-has-next-p)
                   dj--fw-on dj--fw-off)
    (dj--set-state dj--up-btn (dj--not-at-root)
                   dj--up-on dj--up-off)))

(defun dj--history-init ()
  (setq dj--history nil)
  (setq dj--history-size  0)
  (setq dj--history-index 0)
  (dj--history-update-buttons-state))

(defun dj--history-add (element)
  (setq dj--history (nthcdr dj--history-index dj--history)
        dj--history-size (1+ (- dj--history-size dj--history-index))
        dj--history-index 0)
  (push element dj--history)
  (dj--history-update-buttons-state))

(defun dj--history-previous ()
  (when (dj--history-has-previous-p)
    (prog1
        (nth (setq dj--history-index (1+ dj--history-index))
             dj--history)
      (dj--history-update-buttons-state))))

(defun dj--history-next ()
  (when (dj--history-has-next-p)
    (prog1
        (nth (setq dj--history-index (1- dj--history-index))
             dj--history)
      (dj--history-update-buttons-state))))

;;; Callbacks
;;
(defsubst dj--refresh (tree)
  "Refresh the display of the TREE widget."
  ;; Clear children.
  (widget-put tree :args nil)
  (widget-put tree :open t)
  ;; Redraw the tree node.
  (widget-value-set tree (widget-value tree)))

(defun dj--view (dir)
  "Display DIR as the root directory tree."
  (let* ((dir (expand-file-name "" (file-name-as-directory dir)))
         (lnk (file-symlink-p dir))
         (tag (if lnk
                  (file-name-as-directory lnk)
                (file-name-as-directory dir))))
    ;;(widget-put (widget-get dj--tree :node) :button-face nil)
    (widget-put (widget-get dj--tree :node) :tag  tag)
    (widget-put (widget-get dj--tree :node) :link lnk)
    (widget-put (widget-get dj--tree :node) :file dir)
    ;; Update state of the Up button.
    (dj--history-update-buttons-state)
    ;; Redraw the tree.
    (dj--refresh dj--tree)))

(defun dj-file-action (widget &optional _event)
  "Open the file passed in WIDGET.
Optional argument EVENT is the input event received."
  (find-file-other-window (widget-get widget :file)))

(defun dj-view-or-refresh-dir (widget &optional event)
  "View as root or refresh the directory passed in WIDGET's parent.
Optional argument EVENT is the input event received."
  (let* ((tree (widget-get widget :parent))
         (dir (dj--dir tree)))
    (if (memq (event-basic-type event) '(mouse-2 down-mouse-2))
        (progn
          ;; Add new root dir to the history.
          (dj--history-add dir)
          ;; View new root dir.
          (dj--view dir))
      ;; Refresh TREE.
      (dj--refresh tree))))

(defun dj-expand-dir (tree)
  "Expand the tree widget TREE.
Return a list of child widgets."
  (unless (widget-get tree :args)
    (let ((tag (widget-get (widget-get tree :node) :tag))
          (dir (dj--dir tree)))
      (message "Reading directory %s..." tag)
      (condition-case err
          (prog1
              (dj--dir-as-widgets dir)
            (message "Reading directory %s...done" tag))
        (error
         (message "%s" (error-message-string err))
         nil)))))

;;; Command
;;
(defvar dj-dialog-mode-map
  (let ((km (make-sparse-keymap)))
    (set-keymap-parent km widget-keymap)
    (define-key km "n" 'dj-go-forward)
    (define-key km "p" 'dj-go-back)
    (define-key km "u" 'dj-go-up)
    (define-key km "h" 'dj-go-home)
    (define-key km "q" 'dj-dialog-quit)
    (define-key km [down-mouse-1] 'tree-widget-button-click)
    km)
  "Keymap used in the dj dialog.")

(easy-menu-define dj-menu dj-dialog-mode-map
  "Menu shown when in the dj dialog."
  (list "DJ"
        ["Home" dj-go-home
         :active (dj--not-at-home)
         ]
        ["Back" dj-go-back
         :active (dj--history-has-previous-p)
         ]
        ["Forward" dj-go-forward
         :active (dj--history-has-next-p)
         ]
        ["Up" dj-go-up
         :active (dj--not-at-root)
         ]
        ["Quit" dj-dialog-quit
         :active t
         ]
   ))

(defvar dj-tool-bar-map nil)

(defun dj--tool-bar ()
  (or dj-tool-bar-map
      (let ((km (make-sparse-keymap)))
        (when (fboundp 'define-key-after)
          (define-key-after km "h"
            `(menu-item
              "Home" dj-go-home
              :image ,dj--home-on
              :enable (dj--not-at-home)
              ))
          (define-key-after km "p"
            `(menu-item
              "Go Back" dj-go-back
              :image ,dj--bw-on
              :enable (dj--history-has-previous-p)
              ))
          (define-key-after km "n"
            `(menu-item
              "Go Forward" dj-go-forward
              :image ,dj--fw-on
              :enable (dj--history-has-next-p)
              ))
          (define-key-after km "u"
            `(menu-item
              "Go Up" dj-go-up
              :image ,dj--up-on
              :enable (dj--not-at-root)
              ))
          (define-key-after km "q"
            `(menu-item
              "Quit" dj-dialog-quit
              :image ,dj--quit-on
              :enable t
              )))
        (setq dj-tool-bar-map km))))

(defun dj--make-mouse-keymap (callback)
  "Return a keymap that call CALLBACK on mouse events.
CALLBACK is passed the received mouse event."
  (let ((keymap (make-sparse-keymap)))
    ;; Pass mouse-1 events to CALLBACK.
    (define-key keymap [header-line down-mouse-1] 'ignore)
    (define-key keymap [header-line mouse-1] callback)
    (define-key keymap [header-line down-mouse-2] 'ignore)
    (define-key keymap [header-line mouse-2] 'ignore)
    (define-key keymap [header-line down-mouse-3] 'ignore)
    (define-key keymap [header-line mouse-3] 'ignore)
    keymap))

(defun dj--make-button-keymap (command)
  "Return a keymap to run COMMAND on a mouse-1 click event."
  (let ((event (make-symbol "event")))
    (dj--make-mouse-keymap
     `(lambda (,event)
        (interactive "@e")
        (,command)))))

(defconst dj--b-km (dj--make-button-keymap 'dj-go-back))
(defconst dj--f-km (dj--make-button-keymap 'dj-go-forward))
(defconst dj--u-km (dj--make-button-keymap 'dj-go-up))
(defconst dj--h-km (dj--make-button-keymap 'dj-go-home))
(defconst dj--q-km (dj--make-button-keymap 'dj-dialog-quit))

(defun dj--header-line ()
  (list
   "  "
   (apply #'propertize "[Home]"
          (if (dj--not-at-home)
              (list 'display dj--home-on
                    'mouse-face 'mode-line-highlight
                    'pointer 'hand
                    'local-map dj--h-km
                    'help-echo "Home")
            (list 'display dj--home-off
                  'pointer 'arrow)))
   "  "
   (apply #'propertize "[Back]"
          (if (dj--history-has-previous-p)
              (list 'display dj--bw-on
                    'mouse-face 'mode-line-highlight
                    'pointer 'hand
                    'local-map dj--b-km
                    'help-echo "Go back")
            (list 'display dj--bw-off
                  'pointer 'arrow)))
   "  "
   (apply #'propertize "[Forward]"
          (if (dj--history-has-next-p)
              (list 'display dj--fw-on
                    'mouse-face 'mode-line-highlight
                    'pointer 'hand
                    'local-map dj--f-km
                    'help-echo "Go forward")
            (list 'display dj--fw-off
                  'pointer 'arrow)))
   "  "
   (apply #'propertize "[Up]"
          (if (dj--not-at-root)
              (list 'display dj--up-on
                    'mouse-face 'mode-line-highlight
                    'pointer 'hand
                    'local-map dj--u-km
                    'help-echo "Go up")
            (list 'display dj--up-off
                  'pointer 'arrow)))
   "  "
   (propertize "[Quit]"
               'display dj--quit-on
               'mouse-face 'mode-line-highlight
               'pointer 'hand
               'local-map dj--q-km
               'help-echo "Quit")
   ))

(defun dj--widget-bar ()
  (let* ((bbar (apply #'widget-create
                      `(group :format "%v"
                        (push-button :format " %[%t%]"
                                     :action dj-go-home
                                     :help-echo "Home"
                                     :tag-glyph ,dj--home-on
                                     "Home")
                        (push-button :format " %[%t%]"
                                     :action dj-go-back
                                     :help-echo "Go back"
                                     "Back")
                        (push-button :format " %[%t%]"
                                     :action dj-go-forward
                                     :help-echo "Go forward"
                                     "Forward")
                        (push-button :format " %[%t%]"
                                     :action dj-go-up
                                     :help-echo "Go up"
                                     "Up")
                        (push-button :format " %[%t%]"
                                     :action dj-dialog-quit
                                     :help-echo "Quit"
                                     :tag-glyph ,dj--quit-on
                                     "Close")
                        )))
         (btns (widget-get bbar :children)))
    (widget-insert "\n")
    (setq dj--home-btn (nth 0 btns))
    (setq dj--bw-btn   (nth 1 btns))
    (setq dj--fw-btn   (nth 2 btns))
    (setq dj--up-btn   (nth 3 btns))))

(defun dj-dialog-mode ()
  "Major mode of the dj dialog.

\\{dj-dialog-mode-map}"
  (interactive)
  (kill-all-local-variables)
  (setq major-mode 'dj-dialog-mode)
  (setq mode-name "DJ")
  (use-local-map dj-dialog-mode-map)
  (setq truncate-lines t)
  (if (fboundp 'run-mode-hooks)
      (run-mode-hooks 'dj-dialog-mode-hook)
    (run-hooks 'dj-dialog-mode-hook)))

(defun dj-dialog-quit (&rest _args)
  "Close the current dialog.
Ignore ARGS arguments."
  (interactive)
  (kill-buffer (current-buffer)))

(defun dj-go-back (&rest _args)
  (interactive)
  (when (dj--history-has-previous-p)
    (dj--view (dj--history-previous))))

(defun dj-go-forward (&rest _args)
  (interactive)
  (when (dj--history-has-next-p)
    (dj--view (dj--history-next))))

(defun dj-go-up (&rest _args)
  (interactive)
  (let ((up (expand-file-name ".." (dj--dir dj--tree))))
    (dj--history-add up)
    (dj--view up)))

(defun dj-go-home (&rest _args)
  (interactive)
  (let ((home (expand-file-name "~")))
    (dj--history-add home)
    (dj--view home)))

;; (defun dj--after-toggle (tree)
;;   "Hook run after toggling a TREE widget expansion.
;; When folded, force a refresh the TREE when unfolded."
;;   (and (widget-get tree :expander)
;;        (not (widget-get tree :open))
;;        (widget-put tree :args nil)))

(defun dj (dir)
  "Show a tree view of the directory DIR."
  (interactive "DDirectory: ")
  (with-current-buffer (get-buffer-create "*DJ*")
    (erase-buffer)
    (setq buffer-undo-list t)
    (dj-dialog-mode)
    ;; Build the buttons bar.
    (let ((images-found (dj--setup-images)))
      ;; Tool bar/Header Line
      (if (and (boundp 'tool-bar-mode) (boundp 'header-line-format))
          (if (and images-found tool-bar-mode)
              (setq-local tool-bar-map (dj--tool-bar)
                          header-line-format nil)
            (setq-local header-line-format '(:eval (dj--header-line))))
        ;; Widgets
        (dj--widget-bar)))
    ;; Build the tree.
    ;; ;; Do not add extra space between elements.
    ;; (setq-local tree-widget-space-width 0)
    (setq dir (expand-file-name dir))
    ;; Create an "empty" root tree widget for now.
    (setq dj--tree (car (widget-get
                         (widget-create
                          'group :format "\n%v" :indent 1
                          '(dj-tree :node (dj-dir-widget)))
                         :children)))
    (goto-char (point-min))
    (widget-move 1)
    ;; Initialize the history.
    (dj--history-init)
    (dj--history-add dir)
;;     (add-hook 'tree-widget-after-toggle-functions
;;               'dj--after-toggle nil t)
    ;; View the directory, and fill the root tree widget.
    (dj--view dir)
    (widget-setup)
    (pop-to-buffer-same-window (current-buffer))
    (when (fboundp 'tabkit-toggle-local-display)
      (tabkit-toggle-local-display -1))
    ))

(provide 'dj)

;;; dj.el ends here
