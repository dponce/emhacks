;;; tabkit-buffer-tab-icon.el --- tab icons -*- lexical-binding:t -*-

;; Copyright (C) 2022 David Ponce

;; Author: David Ponce <da_vid@orange.fr>
;; Maintainer: David Ponce <da_vid@orange.fr>
;; Created: 25 February 2003
;; Keywords: convenience

;; This file is not part of GNU Emacs.

;; This program is free software: you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; This library complements the tabkit library and provides the minor
;; mode `tabkit-buffer-tab-icon-mode' that associates icons to buffer
;; tabs when enabled.
;;
;; When the icon-theme libraries is available, icon images are looked
;; up in the theme defined in `tabkit-image-icon-theme' with
;; height as close as possible to `tabkit-image-height'.
;;
;; When available, the mime-info library is used to provide file mime
;; type icon names associated to buffers visiting files.
;;
;; When no file mime type icon is found, or for tabs associated to
;; buffers not visiting files, or for group tabs, the option
;; `tabkit-buffer-tab-icon-group-icons' defines custom icons
;; associated to both tab groups and to buffer tabs belonging to these
;; groups.  The option `tabkit-buffer-tab-icon-default-icon' defines a
;; global default fallback icon.
;;

;;; Code:

(require 'tabkit)
(require 'mime-info  nil t)
(or (fboundp 'mime-info-file-type)
    (defalias 'mime-info-file-type  'ignore))
(or (fboundp 'mime-info-type-icon)
    (defalias 'mime-info-type-icon  'ignore))

(defcustom tabkit-buffer-tab-icon-default-icon
  '("♈" (:file "emacs"))
  "The default tab icon spec.
That is, a pair (TEXT . IMAGE-SPECS) where TEXT is the icon as
text (emoji, symbol character, ...), and IMAGE-SPECS is an image
specifications."
  :group 'tabkit-buffer
  :type tabkit--icon-widget
  :set #'tabkit--custom-set-and-refresh-elt)

(defcustom tabkit-buffer-tab-icon-group-icons
  '(
    ("Customize"       . ("✨"))
    ("Dired"           . ("📁"))
    ("Terminal"        . ("💻"))
    ("Debugger"        . ("🐾"))
    ("Help"            . ("⛑"))
    ("Mail"            . ("📧"))
    ("File"            . ("📄"))
    ("Process"         . ("⌛"))
    ("Version Control" . ("🗂️"))
    ("Other"           . ("👾"))
    )
  "Group name/icon associations.
This is an a-list of (GROUP-NAME . ICON-SPEC) elements, where
GROUP-NAME is a group name, and ICON-SPEC the associated icon
specification.  The default icon defined in variable
`tabkit-buffer-tab-icon-default-icon' (see also this variable for the
meaning of ICON-SPEC) is used for group names without a specific
associated icon.  Group names are defined in variable
`tabkit-buffer-group-criteria'."
  :group 'tabkit-buffer
  :type `(repeat :tag "Group/Icon associations"
                 (cons :tag ""
                       (string :tag "Group name")
                       ,tabkit--icon-widget))
  :set #'tabkit--custom-set-and-refresh-elt)

(defsubst tabkit--buffer-tab-icon-default-icon ()
  "Return the default tab icon construct.
The returned value is a propertized string for both the text and image
representation of a tab icon."
  (tabkit-with-memo 'tabkit-buffer-tab-icon-default-icon
    (tabkit-picture tabkit-buffer-tab-icon-default-icon)))

(defsubst tabkit--buffer-tab-icon-group-icons ()
  "Return the alist of groups and associated icon constructs."
  (tabkit-with-memo 'tabkit-buffer-tab-icon-group-icons
    (let (icons)
      (dolist (ielt tabkit-buffer-tab-icon-group-icons)
        (push (cons (car ielt)
                    (tabkit-picture (cdr ielt)))
              icons))
      icons)))

(defsubst tabkit--buffer-tab-icon-group (tab)
  "Return the 1st group TAB belongs to."
  (if tabkit-buffer-use-groups
      (tabkit-tabset-name (tabkit-tab-tabset tab))
    (car (tabkit-buffer-group-criteria (tabkit-tab-value tab)))))

(defun tabkit--buffer-tab-icon-group-icon (tab &optional help)
  "Return the icon associated to group TAB.
If non-nil HELP must be a valid help-echo property added to the
construct."
  (let ((group (tabkit--buffer-tab-icon-group tab)))
    (propertize
     (alist-get group
                (tabkit--buffer-tab-icon-group-icons)
                (tabkit--buffer-tab-icon-default-icon)
                nil #'equal)
     'help-echo (or help
                    (tabkit-help-render "Group <k0>%s</k0>" group)))))

(defsubst tabkit--buffer-tab-icon-cache ()
  "Return the cache of mime type icons."
  (tabkit-with-memo 'tabkit-buffer-tab-icon-cache
    (make-hash-table :test 'equal :size 101)))

(defun tabkit--buffer-tab-icon-file-icon (tab)
  "Return the icon of file in TAB or nil if not found."
  (when-let ((file (buffer-file-name))
             (type (mime-info-file-type file))
             (icon (gethash
                    type (tabkit--buffer-tab-icon-cache) 'new)))
    ;; Always refresh the help-echo text.
    (propertize
     (if (eq icon 'new)
         (let ((img (and tabkit-use-images ;; Only when using images.
                         (mime-info-type-icon
                          type tabkit-image-icon-theme
                          (tabkit-image-pixels-height)))))
           (puthash type (if img
                             (propertize
                              " " 'display
                              (tabkit-find-image `((:file ,img))))
                           (tabkit--buffer-tab-icon-group-icon tab ""))
                    (tabkit--buffer-tab-icon-cache)))
       icon)
     'help-echo (tabkit-help-render
                 "Group <k0>%s</k0><br>Type <k0>%s</k0>"
                 (tabkit--buffer-tab-icon-group tab) type))))

(defun tabkit--buffer-tab-icon-buffer-icon (tab)
  "Return the icon associated to single buffer TAB."
  (with-current-buffer (tabkit-tab-value tab)
    (or (tabkit--buffer-tab-icon-file-icon tab)
        (tabkit--buffer-tab-icon-group-icon tab))))

(defun tabkit-buffer-tab-icon (tab)
  "Return an icon for TAB.
That is, a single buffer icon, or a group icon."
  (if (tabkit-buffer-show-groups-p)
      (tabkit--buffer-tab-icon-group-icon tab)
    (tabkit--buffer-tab-icon-buffer-icon tab)))

(defun tabkit--buffer-tab-icon-setup ()
  "Initialize internal data."
  (setf (tabkit-memo 'tabkit-buffer-tab-icon-default-icon) nil)
  (setf (tabkit-memo 'tabkit-buffer-tab-icon-group-icons) nil)
  (setf (tabkit-memo 'tabkit-buffer-tab-icon-cache) nil))

(defun tabkit--buffer-tab-icon-mode-on ()
  "Turn on `tabkit-buffer-tab-icon-mode'."
  (tabkit--buffer-tab-icon-setup)
  (add-hook 'tabkit-after-data-change-hook
            #'tabkit--buffer-tab-icon-setup)
  (setq tabkit-tab-icon-provider #'tabkit-buffer-tab-icon))

(defun tabkit--buffer-tab-icon-mode-off ()
  "Turn off `tabkit-buffer-tab-icon-mode'."
  (setq tabkit-tab-icon-provider nil)
  (remove-hook 'tabkit-after-data-change-hook
               #'tabkit--buffer-tab-icon-setup)
  (tabkit--buffer-tab-icon-setup))

(defvar tabkit-buffer-tab-icon-mode)

(defun tabkit--buffer-tab-icon-mode-toggle ()
  "Toggle `tabkit-buffer-tab-icon-mode'."
  (if (and (tabkit-mode-on-p) tabkit-buffer-tab-icon-mode)
      (tabkit--buffer-tab-icon-mode-on)
    (tabkit--buffer-tab-icon-mode-off)))

(eval-after-load "tabkit"
  (lambda ()
    (add-hook 'tabkit-mode-hook
              #'tabkit--buffer-tab-icon-mode-toggle
              t)))

;;;###autoload
(define-minor-mode tabkit-buffer-tab-icon-mode
  "Toggle tab icon provider to show buffer tab icons.
With prefix argument ARG, turn on if positive, otherwise off.  Return
non-nil if the new state is enabled."
  :group 'tabkit-buffer
  :require 'tabkit
  :global t
  (tabkit--buffer-tab-icon-mode-toggle))

(provide 'tabkit-buffer-tab-icon)

;;; tabkit-buffer-tab-icon.el ends here
