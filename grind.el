;;; grind.el --- Simple Elisp pretty-printer -*- lexical-binding:t -*-

;; Copyright (C) 2002-2025 David Ponce

;; Author: David Ponce <da_vid@orange.fr>
;; Maintainer: David Ponce <da_vid@orange.fr>
;; Created: 31 Mar 2023
;; Keywords: lisp

;; This file is not part of GNU Emacs.

;; This program is free software: you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; This library(*) implements a simple Elisp pretty-printer.
;;
;; (*) grind vt.
;;
;;     1. [MIT and Berkeley; now rare] To prettify hardcopy of code,
;;     especially LISP code, by reindenting lines, printing keywords
;;     and comments in distinct fonts (if available), etc. This usage
;;     was associated with the MacLISP community and is now rare;
;;     prettyprint was and is the generic term for such operations.
;;
;;     The Jargon File (version 4.4.7)
;;     <http://www.catb.org/~esr/jargon/html/G/grind.html>
;;
;; The main function, `grind-to-string', takes an Elisp s-expression
;; and uses `prin1' to write a linear representation of it into a
;; temporary buffer; then, iterates over this linear form and
;; pretty-prints each sub-expression within a given width by calling
;; an ad-hoc printer: a function defined with `grind-defprinter' that
;; process the s-expression at point matching the printer's regular
;; expression.  Default printers are provided for common Elisp
;; s-expressions.

;;; Code:

(eval-when-compile (require 'cl-macs)) ;; For cl-defmacro.

;;; Misc
;;
(defgroup grind nil
  "A simple Elisp pretty-printer."
  :group 'lisp)

(defun grind--debug ()
  "Function to put in a pretty-printer to help debug it."
  (pop-to-buffer (current-buffer))
  (debug))

(defun grind-escape ()
  "Escape the characters ?\\r and ?\\t in strings."
  (save-excursion
    (goto-char (point-min))
    (while (re-search-forward "\\s\"" nil t)
      (let ((beg (progn (backward-char) (point)))
            (end (progn (forward-sexp) (point-marker))))
        (goto-char beg)
        (while (re-search-forward "[\r\t]" end t)
          (let* ((c (char-before (match-end 0)))
                 (r (cdr (assq c '((?\r . "\\r") (?\t . "\\t"))))))
            (replace-match r t t)))
        (goto-char end)))))

;;; Core pretty-printer functions
;;
(defvar grind--printers (make-hash-table :test 'equal :size 47))

(defsubst grind-printer (function-name)
  "Return the pretty-printer of a call expression to FUNCTION-NAME.
If no specific pretty-printer is found, return the generic
pretty-printer `grind-call-form'.
Value can be set with (setf (grind-printer FUNCTION-NAME) VALUE)."
  (or (gethash function-name grind--printers)
      #'grind-call-form))

(gv-define-setter grind-printer (pretty-printer function-name)
  `(puthash ,function-name ,pretty-printer grind--printers))

(defun grind--set-fprinter (pretty-printer &rest function-names)
  "Set the PRETTY-PRINTER of FUNCTION-NAMES."
  (dolist (fn function-names)
    (setf (grind-printer fn) pretty-printer)))

;;;###autoload
(cl-defmacro grind-defprinter ( name &optional docstring &rest body
                                &key for skip &allow-other-keys)
  "Define NAME as a pretty-printer FOR function call expression.
That is, define a function (defun NAME () [DOCSTRING] BODY...) called
with no arguments to pretty-print the expression at point, which is a
call to one of the function named in the value of the `:for' keyword.

If non-nil, the value of the `:skip' keyword must be a list of symbols
that specify when this pretty-printer can be skipped.  The recognized
symbols are:

- `fit', the pretty-printer can be skipped when the sexp at point fits
  into the available width.

- `quot', the pretty-printer can be skipped when the sexp at point is
  prefixed with the Lisp quote character.

\(fn NAME [DOCSTRING] [:for NAMES...] [:skip SYMBOLS...] BODY...)"
  (declare (indent 1) (doc-string 2))
  (let ((qname (macroexp-quote name)))
    (while (keywordp (car body))
      (setq body (cddr body)))
    (and docstring (setq body (cons docstring body)))
    `(progn
       (defun ,name () ,@body)
       (put ,qname :grind--skip ,(macroexp-quote skip))
       (grind--set-fprinter ,qname ,@for))))

;;; Core pretty-print functions
;;
(defconst grind-min-width 40
  "Minimum `grind-width' value.")

(defvar grind-width nil
  "Limit beyond which automatic line-wrapping should happen.
Default is `fill-column'.  Minimum value is `grind-min-width'.")

(defsubst grind-open-paren-p ()
  "Return non-nil if point is at an open parenthesis character."
  (looking-at "\\s(" t))

(defsubst grind-close-paren-p ()
  "Return non-nil if point is at a close parenthesis character."
  (looking-at "\\s)" t))

(defsubst grind-open-list ()
  "Move down into the list after an open parenthesis character."
  (down-list 1))

(defsubst grind-close-list ()
  "Move out of the list after a close parenthesis character."
  (up-list 1))

(defsubst grind-skip-space ()
  "Skip space and non-constituent characters."
  (skip-syntax-forward "-'")
  (skip-chars-forward "@"))

;;; Heuristic to line break if remaining line width is short.
;;
(defconst grind-width-short-ratio 0.25
  "Ratio of remaining line width considered short.")

(defun grind-skip-sexp-maybe-break ()
  "Skip sexp at point and line break if remaining width is short."
  (forward-sexp)
  (and (> (current-column)
          (round (* grind-width (- 1 grind-width-short-ratio))))
       (grind-maybe-newline-and-indent)))

(defun grind-maybe-newline-and-indent ()
  "Insert a newline, then indent.
Does nothing if point is before a close parenthesis or already at the
beginning of line."
  (or (grind-close-paren-p)
      (save-excursion (backward-prefix-chars)
                      (skip-syntax-backward "-")
                      (bolp))
      (newline-and-indent)))

(defsubst grind-sexp-quoted ()
  "Return non-nil if sexp at point quoted.
That is prefixed with the Lisp quote character."
  (eql (char-before) ?'))

(defun grind-sexp-fit (&optional dry-run)
  "Return non-nil if sexp at point fits within `grind-width'.
Move point after sexp, unless DRY-RUN is non-nil."
  (let ((beg (point))
        (nobreak (progn (forward-sexp)
                        (<= (current-column) grind-width))))
    (or (and nobreak (not dry-run))
        (goto-char beg))
    nobreak))

(defun grind-try-sexp (&optional dry-run)
  "Try to pretty-print sexp at point.
Return non-nil if done, or nil if not printed because exceeding
`grind-width'.  If optional argument DRY-RUN is non-nil, doesn't
actually print."
  (or (grind-sexp-fit dry-run)
      (catch 'undo
        (atomic-change-group
          (let ((beg (point))
                (nobreak (progn (grind-sexp t)
                                (<= (current-column) grind-width))))
            (save-excursion
              (end-of-line 0)
              (while (and nobreak (> (point) beg))
                (setq nobreak (<= (current-column) grind-width))
                (end-of-line 0)))
            (if (and nobreak (not dry-run))
                nobreak
              (goto-char beg)
              (throw 'undo nobreak)))))))

(defun grind-sexp-try-nobreak ()
  "Pretty-print the sexp at point on the same line.
If `grind-width' is exceeded, start on a new line.
Return non-nil, if no line break."
  (let ((beg (point)))
    (or (grind-try-sexp) (grind-sexp))
    (= (line-number-at-pos beg) (line-number-at-pos))))

(defun grind-nil-as-list ()
  "If sexp at point is the symbol nil, print it as the empty list.
Return non-nil, if succeeded."
  (grind-skip-space)
  (when (looking-at "\\<nil\\>")
    (replace-match "()" t t)
    t))

(defun grind-sequence ()
  "Pretty-print the sequence of sexps at point.
Pretty-print each sexp on a new line."
  (while (not (grind-close-paren-p))
    (grind-sexp)))

(defun grind-docstring ()
  "Pretty-print the string after point as a doc string.
That is, line break and reprint the string without escaping newline
characters.
Return nil if there is no string after point."
  (grind-skip-space)
  (when (looking-at "\"" t)
    (grind-maybe-newline-and-indent)
    (let ((start (point))
          (str (read (current-buffer)))
          (end (point)))
      (delete-region start end)
      (prin1 str (current-buffer) '(t)))))

(defun grind-keywords ()
  "Pretty-print a sequence of keyword/value pairs at point.
Return non-nil if keyword/value pairs found."
  (let (key end)
    (while (not (or end (grind-close-paren-p)))
      (grind-skip-space)
      (cond
       ;; Previous keyword expecting a value.
       (key
        (grind-sexp-try-nobreak) ;; PP value.
        (setq key nil)) ;; No value expected.
       ;; New keyword.
       ((looking-at "\\_<:\\(\\(\\sw\\|\\s_\\)+\\)\\_>")
        (grind-maybe-newline-and-indent)
        (forward-sexp)  ;; Skip keyword.
        (setq key t))   ;; Value expected.
       ;; Neither new keyword nor value expected, stop.
       ((setq end t))))
    end))

(defun grind-funspec ()
  "Pretty-print common function spec at point.
That is, ARGLIST [DOCSTRING] [DECL] [INTERACTIVE] BODY...)"
  ;; Print empty arglist as () instead of nil
  (grind-list)
  (grind-docstring)
  (grind-sequence)
  (grind-close-list)
  t)

(defun grind-letspec ()
  "Pretty-print common let form spec at point.
That is, VARLIST BODY...)."
  (grind-skip-space)
  ;; VARLIST
  (unless (grind-nil-as-list)
    (grind-open-list)
    (while (not (grind-close-paren-p))
      (if (not (grind-open-paren-p))
          ;; Symbol alone.
          (forward-sexp)
        (grind-open-list)
        (grind-sexp t)
        (or (grind-close-paren-p)
            (grind-sexp-try-nobreak))
        (grind-close-list))
      (grind-maybe-newline-and-indent))
    (grind-close-list))
  ;; BODY...
  (grind-sequence)
  (grind-close-list))

(defun grind-condspec ()
  "Pretty-print common cond form spec at point.
That is, ((COND [CODE]) ...)."
  (while (not (grind-close-paren-p))
    (grind-maybe-newline-and-indent)
    (grind-open-list)
    (grind-sexp t)
    (grind-sequence)
    (grind-close-list))
  (grind-close-list))

(defun grind-modespec ()
  "Pretty-print common define-*-mode form spec at point.
That is, [DOCSTRING] [KEYWORD-ARGS...] BODY...)."
  (when (grind-docstring)
    (grind-keywords)
    (grind-sequence))
  (grind-close-list))

(defun grind-funcall (pretty-printer)
  "Call PRETTY-PRINTER for sexp at point.
Return non-nil, if succeeded."
  ;; Let's signal an error if below property is invalid!
  (let ((skip (get pretty-printer :grind--skip)))
    (cond
     ((and (memq 'quot skip) (grind-sexp-quoted)) nil)
     ((and (memq 'fit skip) (grind-sexp-fit)))
     ((catch 'undo
        (ignore-errors
          (atomic-change-group
            (or (funcall pretty-printer)
                (throw 'undo nil)))))))))

(defun grind-sexp (&optional nobreak)
  "Pretty-print sexp at point.
Start on a new line, unless optional argument NOBREAK is non-nil."
  (or nobreak (grind-maybe-newline-and-indent))
  (let ((end-mark (save-excursion (forward-sexp) (point-marker))))
    (while (< (point) (marker-position end-mark))
      (grind-skip-space)
      (cond
       ;; A function call.
       ((and (looking-at "(\\_<\\(\\(\\sw\\|\\s_\\)+\\)\\_>")
             (grind-funcall (grind-printer (match-string 1)))))
       ;; A list or vector.
       ((and (grind-open-paren-p)
             (grind-funcall #'grind-list)))
       ;; An atom.
       ((goto-char (marker-position end-mark)))))
    (set-marker end-mark nil)))

;;; Core pretty-printers
;;
(grind-defprinter grind-list
  "Pretty-print the list at point."
  (cond
   ((grind-nil-as-list))
   ((grind-sexp-fit))
   (t
    (grind-open-list)
    (while (not (grind-close-paren-p))
      (unless (grind-sexp-fit)
        (grind-maybe-newline-and-indent)
        (grind-skip-space)
        (if (grind-open-paren-p)
            (grind-list)
          (forward-sexp))))
    (grind-close-list)))
  t)

(grind-defprinter grind-call-form
  "Pretty-print the function call form at point."
  :skip (fit quot)
  (when-let* ((fun (intern-soft (match-string 1)))
              ;; ((or (eq fun 'closure) (fboundp fun)))
              )
    (let* ((indent (get fun 'lisp-indent-function))
           (docelt (get fun 'doc-string-elt))
           (nobreak (cond
                     ((eq indent 'defun) 2)
                     (indent)
                     ;; If not specified, assume that all parameters
                     ;; are requested to be on the same line.
                     (most-positive-fixnum)))
           (elt 1))
      (grind-open-list)
      ;; Line break after a long function name.
      (grind-skip-sexp-maybe-break)
      (unless (grind-close-paren-p)
        ;; Try to put the first parameter on the same line if
        ;; lisp-indent-function is > 0.
        (when (> nobreak 0)
          (setq nobreak (if (grind-sexp-try-nobreak) (1- nobreak) 0)
                elt (1+ elt)))
        ;; Then other parameters requested to be on the same line.
        (while (and (> nobreak 0)
                    (not (grind-close-paren-p))
                    (grind-sexp-fit))
          (setq nobreak (1- nobreak)
                elt (1+ elt)))
        ;; Assume doc-string-elt is > lisp-indent-function.
        (when (eql elt docelt)
          (grind-docstring)
          (grind-maybe-newline-and-indent))
        ;; Then, remaining parameters.
        (if indent
            ;; If some of the first parameters have a specific
            ;; indentation, put others on separate lines.
            (grind-sequence)
          ;; Otherwise, put as many as possible on same line.
          (or (> nobreak 0) (grind-maybe-newline-and-indent))
          (while (not (grind-close-paren-p))
            (unless (grind-sexp-fit)
              (grind-maybe-newline-and-indent)
              (or (grind-sexp-try-nobreak)
                  (grind-maybe-newline-and-indent))))))
      (grind-close-list))
    t))

;;; Default pretty-printers
;;
(grind-defprinter grind-closure
  "Default printer for `closure' like forms."
  :for ("closure")
  :skip (quot)
  (grind-open-list)
  (forward-sexp)
  (grind-maybe-newline-and-indent)
  (grind-list) ;; env
  (grind-maybe-newline-and-indent)
  (grind-funspec))

(grind-defprinter grind-lambda
  "Default printer for `lambda' like forms."
  :for ("lambda")
  (grind-open-list)
  (forward-sexp)
  (grind-funspec))

(grind-defprinter grind-defun
  "Default printer for `defun' like forms."
  :for ("defun" "defmacro" "defsubst" "cl-defgeneric")
  :skip (quot)
  (grind-maybe-newline-and-indent)
  (grind-open-list)
  (forward-sexp) ;; DEF...
  (forward-sexp) ;; name
  (grind-funspec))

(grind-defprinter grind-defmethod
  "Default printer for `cl-defmethod' like forms."
  :for ("cl-defmethod")
  :skip (quot)
  (grind-maybe-newline-and-indent)
  (grind-open-list)
  (forward-sexp) ;; DEF...
  (forward-sexp) ;; name
  ;; Qualifiers.
  (grind-skip-space)
  (while (not (grind-open-paren-p))
    (grind-sexp-try-nobreak)
    (grind-skip-space))
  ;; Rest is common funspec.
  (grind-funspec))

(grind-defprinter grind-defvar
  "Default printer for `defvar' like forms."
  :for ("defconst" "defvar" "defcustom" "defgroup"
         "defvaralias" "defvar-local" "defface")
  :skip (quot)
  (grind-maybe-newline-and-indent)
  (grind-open-list)
  (forward-sexp) ;; DEF...
  (forward-sexp) ;; name
  (unless (grind-close-paren-p)
    (grind-sexp-try-nobreak) ;; value
    (and (grind-docstring)   ;; doc
         (grind-keywords)))  ;; keywords
  (grind-close-list)
  t)

(grind-defprinter grind-defminormode
  "Default printer for `define-minor-mode' like forms."
  :for ("define-minor-mode")
  :skip (quot)
  (grind-maybe-newline-and-indent)
  (grind-open-list)
  (forward-sexp) ;; DEF...
  (forward-sexp) ;; mode
  (grind-modespec)
  t)

(grind-defprinter grind-defmajormode
  "Default printer for `define-derived-mode' like forms."
  :for ("define-derived-mode")
  :skip (quot)
  (grind-maybe-newline-and-indent)
  (grind-open-list)
  (forward-sexp) ;; DEF...
  (forward-sexp) ;; child
  (grind-sexp-try-nobreak) ;; parent
  (grind-sexp-try-nobreak) ;; name
  (grind-modespec)
  t)

(grind-defprinter grind-let
  "Default printer for `let' like forms."
  :for ("let" "let*" "letrec" "when-let" "when-let*")
  :skip (quot fit)
  (grind-open-list)
  (forward-sexp)
  (grind-letspec)
  t)

(grind-defprinter grind-named-let
  "Default printer for `named-let' like forms."
  :for ("named-let")
  :skip (quot fit)
  (grind-open-list)
  (forward-sexp)
  (forward-sexp) ;; name
  (grind-letspec)
  t)

(grind-defprinter grind-if
  "Default printer for `if' like forms."
  :for ("if" "if-let" "if-let*")
  :skip (quot fit)
  (grind-open-list)
  (forward-sexp)
  (grind-sexp t)
  (grind-maybe-newline-and-indent)
  (grind-sequence)
  (grind-close-list)
  t)

(grind-defprinter grind-while
  "Default printer for `while' like forms."
  :for ("while" "when" "unless" "condition-case" "catch"
         "dotimes" "dolist")
  :skip (quot fit)
  (grind-open-list)
  (forward-sexp)
  (grind-sexp t)
  (grind-sequence)
  (grind-close-list)
  t)

(grind-defprinter grind-progn
  "Default printer for `progn' like forms."
  :for (;; "prog1" "prog2"
         "progn" "unwind-protect")
  :skip (quot fit)
  (grind-open-list)
  (forward-sexp)
  (grind-sequence)
  (grind-close-list)
  t)

(grind-defprinter grind-setq
  "Default printer for `setq' like forms."
  :for ("setq" "setq-local" "set")
  :skip (quot fit)
  (grind-open-list)
  (forward-sexp)           ;; SET...
  (forward-sexp)           ;; 1st VA
  (grind-sexp-try-nobreak) ;; 1st VAL
  (while (not (grind-close-paren-p))
    (grind-maybe-newline-and-indent)
    (forward-sexp)            ;; VAR
    (grind-sexp-try-nobreak)) ;; VAL
  (grind-close-list)
  t)

(grind-defprinter grind-cond
  "Default printer for `cond' like forms."
  :for ("cond")
  :skip (quot fit)
  (grind-open-list)
  (forward-sexp)
  (grind-condspec)
  t)

(grind-defprinter grind-pcase
  "Default printer for `pcase' like forms."
  :for ("pcase")
  :skip (quot fit)
  (grind-open-list)
  (forward-sexp)
  (grind-sexp t)
  (grind-condspec)
  t)

;;; Entry points
;;
;;;###autoload
(defun grind-to-string (object &optional width)
  "Return the pretty-printed representation of OBJECT as a string.
OBJECT can be any Lisp object.  Quoting characters are used as needed
to make output that `read' can handle, whenever this is possible.
Optional argument WIDTH, which defaults to `fill-column', is the limit
beyond which automatic line-wrapping should happen."
  (setq width (1+ (max (or width fill-column 70) grind-min-width)))
  (let ((buffer (generate-new-buffer "*grind*" t)))
    (with-current-buffer buffer
      (condition-case err
          (progn
            (buffer-disable-undo)
            (emacs-lisp-mode)
            (prin1 object (current-buffer)
                   '(t (escape-newlines . t)
                       (integers-as-characters . t)))
            (goto-char (point-min))
            (let ((grind-width width)
                  (inhibit-modification-hooks t))
              (grind-sexp)
              (grind-escape))
            (prog1 (buffer-string) (kill-buffer buffer)))
        ((debug error)
         (message "%s" (error-message-string err))
         (pop-to-buffer buffer)
         (buffer-string))))))

;;;###autoload
(defun grind (object &optional width printcharfun)
  "Pretty-print OBJECT to output stream.
Optional argument WIDTH, which defaults to `fill-column', is the limit
beyond which automatic line-wrapping should happen.
See also `princ' for the specification of PRINTCHARFUN."
  (princ (grind-to-string object width)
         (or printcharfun standard-output)))

;;;###autoload
(defun grind-function (function-name &optional width)
  "Pretty-print FUNCTION-NAME on a temporary buffer.
Optional argument WIDTH, which defaults to `fill-column', is the limit
beyond which automatic line-wrapping should happen.
Signal an error if FUNCTION-NAME is byte-compiled."
  (interactive "agrind function name: ")
  (let ((code (symbol-function function-name)))
    (if (byte-code-function-p code)
        (user-error "Can't pretty-print a byte compiled function")
      (with-current-buffer
          (get-buffer-create (format "*grind %s*" function-name))
        (erase-buffer)
        (grind code width (current-buffer))
        (goto-char (point-min))
        (emacs-lisp-mode)
        (pop-to-buffer-same-window (current-buffer))))))

;;;###autoload
(defun grind-insert (form &optional width)
  "Insert pretty-printed FORM at point in current buffer.
Optional argument WIDTH, which defaults to `fill-column', is the limit
beyond which automatic line-wrapping should happen."
  (insert (grind-to-string form width) ?\n))

;;;###autoload
(defun grind-macroexpand (form &optional environment width)
  "Pretty-print result of expanding macros at top levels in FORM.
Result is inserted at point in current buffer.
See `macroexpand' for more details about FORM and ENVIRONMENT.
Optional argument WIDTH, which defaults to `fill-column', is the limit
beyond which automatic line-wrapping should happen."
  (grind-insert (macroexpand form environment) width))

;;;###autoload
(defun grind-macroexpand-all (form &optional environment width)
  "Pretty-print result of expanding macros at all level of FORM.
Result is inserted at point in current buffer.
See `macroexpand-all' for more details about FORM and ENVIRONMENT.
Optional argument WIDTH, which defaults to `fill-column', is the limit
beyond which automatic line-wrapping should happen."
  (grind-insert (macroexpand-all form environment) width))

(defun grind--read-parenthesized-sexp ()
  "Return parenthesized sexp at point in current buffer.
That is, the sexp:

 * that starts at the open parenthesis at point;
 * that ends at the close parenthesis just before point;
 * inside the level of parentheses surrounding point.

Return nil otherwise."
  (save-excursion
    (when-let* ((p (point))
                (s (ignore-errors
                     (or (grind-open-paren-p)
                         (progn (backward-sexp) (grind-open-paren-p))
                         (progn (goto-char p) (up-list -1)))
                     (buffer-substring-no-properties
                      (progn (backward-prefix-chars) (point))
                      (progn (forward-sexp) (point))))))
      (read s))))

;;;###autoload
(defun grind-at-point (&optional expand)
  "Run `grind' on the parenthesized sexp at point in current buffer.
Expand macros at all levels if EXPAND is a positive number, or at top
level if EXPAND is a negative number, and pop to a working buffer
where the result is pretty-printed."
  (interactive "P")
  (when-let* ((sexp (grind--read-parenthesized-sexp)))
    (let ((lb lexical-binding))
      (with-current-buffer (get-buffer-create "*grind*")
        (erase-buffer)
        ;; Propagate to working buffer the `lexical-binding' setting
        ;; for macro expansion.
        (setq lexical-binding lb)
        (cond ((consp expand) (setq expand (car expand)))
              ((eq expand '-) (setq expand -1)))
        (funcall (if (numberp expand)
                     (cond ((<  expand 0) #'grind-macroexpand)
                           ((>= expand 0) #'grind-macroexpand-all)
                           (t #'grind-insert))
                   #'grind-insert)
                 sexp)
        (goto-char (point-min))
        (emacs-lisp-mode)
        (pop-to-buffer-same-window (current-buffer))))))

;;;###autoload
(defun grind-at-point-macroexpand ()
  "Run `grind' on the parenthesized sexp at point in current buffer.
Expand macros at top level and pop to a working buffer where the
result is pretty-printed."
  (interactive)
  (grind-at-point -1))

;;;###autoload
(defun grind-at-point-macroexpand-all ()
  "Run `grind' on the parenthesized sexp at point in current buffer.
Expand macros at all levels and pop to a working buffer where the
result is pretty-printed."
  (interactive)
  (grind-at-point 1))

(provide 'grind)

;;; grind.el ends here
